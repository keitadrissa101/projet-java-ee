package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.Image;
import controller.ImageMgr;
import controller.CategoryMgr;
import model.Categorie;
import controller.ProduitMgr;
import java.util.ArrayList;
import model.Produit;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head> \n");
      out.write("    <body>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "views/header.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("        ");

            ArrayList<Produit> l1 = new ArrayList();
            ProduitMgr cmg = new ProduitMgr();
            l1 = cmg.list2();
          
      out.write("\n");
      out.write("            \n");
      out.write("        <section id=\"slider\"><!--slider-->\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-sm-12\">\n");
      out.write("                        <div id=\"slider-carousel\" class=\"carousel slide\" data-ride=\"carousel\">\n");
      out.write("                            <ol class=\"carousel-indicators\">\n");
      out.write("                                ");
 for (int i = 0; i <l1.size(); i++) { 
      out.write("\n");
      out.write("                                <li data-target=\"#slider-carousel\" data-slide-to=\"");
      out.print(i);
      out.write('"');
      out.print((i==0)?"class=\"active\"":"");
      out.write("></li>\n");
      out.write("                                ");
}
      out.write("\n");
      out.write("                            </ol>\n");
      out.write("\n");
      out.write("                            <div class=\"carousel-inner\">\n");
      out.write("                               ");

                                   int i=0;
                                for (Produit p : l1) {

                String cat = "";
                switch (p.getIdcat()) {
                    case 1:
                        cat = "Respiratory";
                        break;
                    case 2:
                        cat = "Analgesics";
                        break;
                    case 3:
                        cat = "Gastrointestinal";
                        break;
                    case 4:
                        cat = "Circulatory";
                        break;
                    case 5:
                        cat = "Eye/ear";
                        break;
                    case 6:
                        cat = "Oral hygienne";
                        break;
                    case 7:
                        cat = "Amtiparasitic";
                        break;
                }

                ImageMgr mrg = new ImageMgr();
                Image img = mrg.rechercher(p.getIdp());
        
      out.write("\n");
      out.write("                                <div class=\"item ");
      out.print((i==0)?" active":"");
      out.write("\">\n");
      out.write("                                    <div class=\"col-sm-6\">\n");
      out.write("                                        <h1><span>");
      out.print(p.getLibelle());
      out.write("</span></h1>\n");
      out.write("                                        <p>");
      out.print(p.getDescription());
      out.write("</p>\n");
      out.write("                                         <a href=\"/myMedicineApp/ManageCart?action=ajouter&idp=");
      out.print(p.getIdp());
      out.write("&qte=1\" class=\"btn btn-default\">Get it now</a>\n");
      out.write("                                        \n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"col-sm-6\">\n");
      out.write("                                        <img src=\"");
      out.print(img.getUrl());
      out.write("\" class=\"girl img-responsive\" alt=\"\" />\n");
      out.write("\t\t\t\t        <img src=\"images/home/pricing.png\"  class=\"pricing\" alt=\"\" />\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                ");

                                   i++;
                                    }
                                
      out.write("\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <a href=\"#slider-carousel\" class=\"left control-carousel hidden-xs\" data-slide=\"prev\">\n");
      out.write("                                <i class=\"fa fa-angle-left\"></i>\n");
      out.write("                            </a>\n");
      out.write("                            <a href=\"#slider-carousel\" class=\"right control-carousel hidden-xs\" data-slide=\"next\">\n");
      out.write("                                <i class=\"fa fa-angle-right\"></i>\n");
      out.write("                            </a>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section><!--/slider-->\n");
      out.write("        ");

            ArrayList<Categorie> lc = new ArrayList();
            CategoryMgr ctg = new CategoryMgr();
            lc = ctg.list();


        
      out.write("\n");
      out.write("        <section>\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-sm-3\">\n");
      out.write("                        <div class=\"left-sidebar\">\n");
      out.write("                            <h2>Category</h2>\n");
      out.write("                            <div class=\"panel-group category-products\" id=\"accordian\"><!--category-productsr-->\n");
      out.write("\n");
      out.write("\n");
      out.write("                            ");
 
                                for (Categorie c : lc) {
                            
      out.write("\n");
      out.write("\n");
      out.write("                            <div class=\"panel panel-default\">\n");
      out.write("                                <div class=\"panel-heading\">\n");
      out.write("                                    <h4 class=\"panel-title\"><a href=\"/myMedicineApp/search.jsp?idc=");
      out.print(c.getIdcat());
      out.write("&mot=\">");
      out.print(c.getLibelle());
      out.write("</a></h4>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            ");
                                                             }
                            
      out.write("\n");
      out.write("\n");
      out.write("                            </div><!--/category-products-->\n");
      out.write("\n");
      out.write("                            <div class=\"brands_products\"><!--brands_products-->\n");
      out.write("                                <h2>Made from </h2>\n");
      out.write("                                <div class=\"brands-name\">\n");
      out.write("                                    <ul class=\"nav nav-pills nav-stacked\">\n");
      out.write("                                        <li><a href=\"#\"> <span class=\"pull-right\">(50)</span>China</a></li>\n");
      out.write("                                        <li><a href=\"#\"> <span class=\"pull-right\">(56)</span>USA</a></li>\n");
      out.write("                                        <li><a href=\"#\"> <span class=\"pull-right\">(27)</span>India</a></li>\n");
      out.write("                                        <li><a href=\"#\"> <span class=\"pull-right\">(32)</span>France</a></li>\n");
      out.write("                                        <li><a href=\"#\"> <span class=\"pull-right\">(5)</span>Italia</a></li>\n");
      out.write("                                        <li><a href=\"#\"> <span class=\"pull-right\">(9)</span>Nigeria</a></li>\n");
      out.write("                                    </ul>\n");
      out.write("                                </div>\n");
      out.write("                            </div><!--/brands_products-->\n");
      out.write("\n");
      out.write("                            <div class=\"price-range\"><!--price-range-->\n");
      out.write("                                <h2>Price Range</h2>\n");
      out.write("                                <div class=\"well text-center\">\n");
      out.write("                                    <input type=\"text\" class=\"span2\" value=\"\" data-slider-min=\"0\" data-slider-max=\"600\" data-slider-step=\"5\" data-slider-value=\"[250,450]\" id=\"sl2\" ><br />\n");
      out.write("                                    <b class=\"pull-left\">$ 0</b> <b class=\"pull-right\">$ 600</b>\n");
      out.write("                                </div>\n");
      out.write("                            </div><!--/price-range-->\n");
      out.write("\n");
      out.write("                            <div class=\"shipping text-center\"><!--shipping-->\n");
      out.write("                                <img src=\"images/home/shipping.jpg\" alt=\"\" />\n");
      out.write("                            </div><!--/shipping-->\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"col-sm-9 padding-right\">\n");
      out.write("                        <div class=\"features_items\"><!--features_items-->\n");
      out.write("                            <h2 class=\"title text-center\">Features Items</h2>\n");
      out.write("                            ");

                                ArrayList<Produit> l2 = new ArrayList();
                                ProduitMgr cmg2 = new ProduitMgr();
                                l2 = cmg2.list2();

                                for (Produit p2 : l2) {

                                    String cat = "";
                                    switch (p2.getIdcat()) {
                                        case 1:
                                            cat = "Respiratory";
                                            break;
                                        case 2:
                                            cat = "Analgesics";
                                            break;
                                        case 3:
                                            cat = "Gastrointestinal";
                                            break;
                                        case 4:
                                            cat = "Circulatory";
                                            break;
                                        case 5:
                                            cat = "Eye/ear";
                                            break;
                                        case 6:
                                            cat = "Oral hygienne";
                                            break;
                                        case 7:
                                            cat = "Amtiparasitic";
                                            break;
                                    }
                                    int idp2 = p2.getIdp();
                                   ImageMgr mrg2 = new ImageMgr();
                                   Image img2 = mrg2.rechercher(idp2);
                            
      out.write("\n");
      out.write("                            <div class=\"col-sm-4\">\n");
      out.write("                                <div class=\"product-image-wrapper\">\n");
      out.write("                                    <div class=\"single-products\">\n");
      out.write("                                        <div class=\"productinfo text-center\">\n");
      out.write("                                            <img src=\"");
      out.print(img2.getUrl());
      out.write("\" alt=\"\" />\n");
      out.write("                                            <h2>$");
      out.print(p2.getPrix());
      out.write("</h2>\n");
      out.write("                                            <p>");
      out.print(p2.getLibelle());
      out.write("</p>\n");
      out.write("                                            <a href=\"#\" class=\"btn btn-default add-to-cart\"><i class=\"fa fa-shopping-cart\"></i>");
      out.print(img2.getUrl());
      out.write("</a>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"product-overlay\">\n");
      out.write("                                            <div class=\"overlay-content\">\n");
      out.write("                                                <h2>$");
      out.print(p2.getPrix());
      out.write("</h2>\n");
      out.write("                                                <p>");
      out.print(p2.getLibelle());
      out.write("</p>\n");
      out.write("                                                <a href=\"/myMedicineApp/detailProduct.jsp?idp=");
      out.print(p2.getIdp());
      out.write("&idc=");
      out.print(p2.getIdcat());
      out.write("\" class=\"btn btn-default add-to-cart\"><i class=\"fa fa-eye\"></i>Product details</a>\n");
      out.write("                                                <a href=\"/myMedicineApp/ManageCart?action=ajouter&idp=");
      out.print(p2.getIdp());
      out.write("&qte=1\" class=\"btn btn-default add-to-cart\"><i class=\"fa fa-shopping-cart\"></i>Add to cart</a>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"choose\">\n");
      out.write("                                        <ul class=\"nav nav-pills nav-justified\">\n");
      out.write("                                            <li><a href=\"#\"><i class=\"fa fa-plus-square\"></i>Add to wishlist</a></li>\n");
      out.write("                                            <li><a href=\"#\"><i class=\"fa fa-plus-square\"></i>Add to compare</a></li>\n");
      out.write("                                        </ul>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            ");

                                }
                            
      out.write("\n");
      out.write("\n");
      out.write("                        </div><!--features_items-->\n");
      out.write("\n");
      out.write("                        <div class=\"category-tab\"><!--category-tab-->\n");
      out.write("                            <div class=\"col-sm-12\">\n");
      out.write("                                <ul class=\"nav nav-tabs\">\n");
      out.write("                                    <li class=\"active\"><a href=\"#tshirt\" data-toggle=\"tab\">Nutritional</a></li>\n");
      out.write("                                    <li><a href=\"#blazers\" data-toggle=\"tab\">Antiparasitic</a></li>\n");
      out.write("                                    <li><a href=\"#sunglass\" data-toggle=\"tab\">Gastrointestinal</a></li>\n");
      out.write("                                    <li><a href=\"#kids\" data-toggle=\"tab\">Circulatory</a></li>\n");
      out.write("                                    <li><a href=\"#poloshirt\" data-toggle=\"tab\">Respiratory</a></li>\n");
      out.write("                                </ul>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"tab-content\">\n");
      out.write("                                <div class=\"tab-pane fade active in\" id=\"tshirt\" >\n");
      out.write("                                    <div class=\"col-sm-3\">\n");
      out.write("                                        <div class=\"product-image-wrapper\">\n");
      out.write("                                            <div class=\"single-products\">\n");
      out.write("                                                <div class=\"productinfo text-center\">\n");
      out.write("                                                    <img src=\"images/home/gallery1.jpg\" alt=\"\" />\n");
      out.write("                                                    <h2>$56</h2>\n");
      out.write("                                                    <p>Ointment</p>\n");
      out.write("                                                    <a href=\"#\" class=\"btn btn-default add-to-cart\"><i class=\"fa fa-shopping-cart\"></i>Add to cart</a>\n");
      out.write("                                                </div>\n");
      out.write("\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                    \n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div><!--/category-tab-->\n");
      out.write("\n");
      out.write("                        <div class=\"recommended_items\"><!--recommended_items-->\n");
      out.write("                            <h2 class=\"title text-center\">recommended items</h2>\n");
      out.write("\n");
      out.write("                            <div id=\"recommended-item-carousel\" class=\"carousel slide\" data-ride=\"carousel\">\n");
      out.write("                                <div class=\"carousel-inner\">\n");
      out.write("                                    <div class=\"item active\">\t\n");
      out.write("                                        <div class=\"col-sm-4\">\n");
      out.write("                                            <div class=\"product-image-wrapper\">\n");
      out.write("                                                <div class=\"single-products\">\n");
      out.write("                                                    <div class=\"productinfo text-center\">\n");
      out.write("                                                        <img src=\"images/home/recommend1.jpg\" alt=\"\" />\n");
      out.write("                                                        <h2>$56</h2>\n");
      out.write("                                                        <p>Aspirins</p>\n");
      out.write("                                                        <a href=\"#\" class=\"btn btn-default add-to-cart\"><i class=\"fa fa-shopping-cart\"></i>Add to cart</a>\n");
      out.write("                                                    </div>\n");
      out.write("\n");
      out.write("                                                </div>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                        \n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <a class=\"left recommended-item-control\" href=\"#recommended-item-carousel\" data-slide=\"prev\">\n");
      out.write("                                        <i class=\"fa fa-angle-left\"></i>\n");
      out.write("                                    </a>\n");
      out.write("                                    <a class=\"right recommended-item-control\" href=\"#recommended-item-carousel\" data-slide=\"next\">\n");
      out.write("                                        <i class=\"fa fa-angle-right\"></i>\n");
      out.write("                                    </a>\t\t\t\n");
      out.write("                                </div>\n");
      out.write("                            </div><!--/recommended_items-->\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("        </section>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "views/footer.jsp", out, false);
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
