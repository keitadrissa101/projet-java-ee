<%-- 
    Document   : listCommande
    Created on : 14 déc. 2020, 18:45:42
    Author     : Administrateur
--%>

<%@page import="controller.LignecommandeMgr"%>
<%@page import="model.Commande"%>
<%@page import="java.util.ArrayList"%>
<%@page import="controller.CommandeMgr"%>
<%@page import="model.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        
        <jsp:include page="views/header.jsp"></jsp:include> 
        
        <% 
           if (session.getAttribute("clt")==null) {
                   request.setAttribute("msg", "You must login");
                   request.getRequestDispatcher("login.jsp").forward(request, response);
               }
                 Client c = (Client)session.getAttribute("clt");
                 int idc = c.getIdc();
         
                            if (request.getAttribute("msg")!=null) {
                              
                            %>
                            <h3 style="color: red;"><%=request.getAttribute("msg")%></h3>
                            <%   }
           
           CommandeMgr cmgr = new CommandeMgr();
           
           String req = "SELECT * FROM `commande` WHERE etat='en progress...'";
           
             ArrayList<Commande> cl = new ArrayList();
                                                    
              cl = cmgr.listr(req);

        
        
        %>
        
        
        <section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Customers Orders Liste In Progress</li>
				</ol>
			</div>
                    
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Num Commande</td>
							<td class="price">Date</td>
							<td class="quantity">State</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
                                        <%
                                            for (Commande cm : cl) {
                                            int id = cm.getIdcmd();
                                            LignecommandeMgr lcg = new LignecommandeMgr();
                                            int total = lcg.montant(id);
                                            
                                                
                                            
                                            %>
					<tbody>
                                           
						<tr>
							<td class="cart_product">
                                                            <%=cm.getIdcmd() %>
							</td>
							<td class="cart_description">
								<h4><a href=""> <%=cm.getDatecmd() %></a></h4>
                                                                
							</td>
							<td class="cart_price">
								<p><%=cm.getEtat() %></p>
							</td>
							<td class="cart_total">
                                                            <p class="cart_total_price">$<%=total %></p>
							</td>
							<td class="cart_delete">
                                                            <a class="cart_quantity_delete" href="/myMedicineApp/detailCommande.jsp?idcmd=<%=cm.getIdcmd()%>"><i class="fa fa-eye"></i></a>
                                                            <button class="cart_quantity_delete" data-toggle="modal" data-target="#expModal" data-idcmd="<%=cm.getIdcmd()%>">Expedite</button>
							</td>
						</tr>

						<% } %>
					</tbody>
				</table>
			</div>
                                       
		</div>
	</section> <!--/#cart_items-->
        
        
<div class="modal fade" id="expModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <form action="ExpServ" method="post">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Commande Expedition :</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">       
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Commande ID :</label>
            <input type="text" class="form-control" name="idcmd" id="idcmd">
          </div>
           <div class="form-group">
            <label for="recipient-name" class="col-form-label">Expedition Mean :</label>
            <input type="text" class="form-control" name="moyen" >
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Follow-up Number :</label>
            <textarea class="form-control" name="numsuivi" ></textarea>
          </div>       
    </div>
          <div class="modal-footer">
            <button type="sumit" class="btn btn-primary">Save</button>
          </div>
    </div>
    </form>
  </div>
</div>
        <jsp:include page="views/footer.jsp"></jsp:include>
        <script>
            $('#expModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var idcmd = button.data('idcmd') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
 
  modal.find('#idcmd').val(idcmd)
})

        </script>   
 
</body>
</html>
