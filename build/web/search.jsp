<%-- 
    Document   : search
    Created on : 26 nov. 2020, 11:16:58
    Author     : Administrateur
--%>

<%@page import="controller.ProduitMgr"%>
<%@page import="java.util.List"%>
<%@page import="model.Produit"%>
<%@page import="javax.management.Query"%>
<%@page import="model.Categorie"%>
<%@page import="controller.CategoryMgr"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        
        
        
        <%
          
        ArrayList<Categorie> lc = new ArrayList();
            CategoryMgr ctg = new CategoryMgr();
            lc = ctg.list();
        %>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							
						<% 
                                                    for (Categorie c : lc) {                                                       
                                                      
                                                    %>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="/myMedicineApp/search.jsp?idc=<%=c.getIdcat()%>&mot="><%=c.getLibelle()%></a></h4>
								</div>
							</div>
						    <% 
                                                    }                                                      
                                                    %>
							
						</div><!--/category-products-->
					
						
						
						<div class="price-range"><!--price-range-->
							<h2>Price Range</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
								 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
							</div>
						</div><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<img src="images/home/shipping.jpg"  alt="" />
						</div><!--/shipping-->
					
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Resolt of research</h2>
                                                <% 
                                                    String mot = request.getParameter("mot");
                                                    if (mot==null) {
                                                            mot = "";
                                                        }
                                                    String req = "SELECT * FROM `produit` WHERE libelle like '%"+mot.toLowerCase()+"%'";
                                                    if (request.getParameter("idc")!=null) {
                                                            int idc = Integer.parseInt(request.getParameter("idc"));
                                                            req+=" and idcat="+idc;
                                                        }
                                                    
                                                    ArrayList<Produit> pl = new ArrayList();
                                                    ProduitMgr prg = new ProduitMgr();
                                                    pl = prg.listr(req);
                                                    if (pl.size()<0) {
                                                        %>
                                                        <h2>Not found !!!</h2> 
                                                    <%    }else{
                                                   for (Produit p : pl) {
                                                %>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="images/home/product1.jpg"  alt="" />
											<h2><%=p.getPrix()%>$</h2>
											<p><%=p.getLibelle()%></p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<h2><%=p.getPrix()%>$</h2>
												<p><%=p.getLibelle()%></p>
                                                                                                <a href="/myMedicineApp/detailProduct.jsp?idp=<%=p.getIdp()%>&idc=<%=p.getIdcat()%>" class="btn btn-default add-to-cart"><i class="fa fa-eye"></i>Product details</a>
                                                                                                <a href="/myMedicineApp/ManageCart?action=ajouter&idp=<%=p.getIdp()%>&qte=1" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
						
						
						<% 
                                                    }  

                                                      }
                                                    %>
						
						
						
					</div><!--features_items-->
					
					
					
				</div>
			</div>
		</div>
	</section>
        
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
