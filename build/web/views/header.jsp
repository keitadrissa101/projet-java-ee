<%-- 
    Document   : footer
    Created on : 14 déc. 2020, 16:38:55
    Author     : Administrateur
--%>

<%@page import="model.Compte"%>
<%@page import="controller.CompteMgr"%>
<%@page import="model.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Pharma</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">      
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <% 
              Client clt = null;
              int idcpt;
              String role = "visiter";
           if (session.getAttribute("clt")!=null) {
                   clt = (Client)session.getAttribute("clt");
                    idcpt = clt.getIdcpt();
                   CompteMgr cpmg = new CompteMgr();
                   Compte cp = cpmg.rechercher(idcpt);
                   role = cp.getRole();
               }
            
        %>
    <header id="header"><!--header-->
        <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            <ul class="nav nav-pills">
                                <li><a href="#"><i class="fa fa-phone"></i> +223 77 77 77 77</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i> groupII@java.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="social-icons pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->

        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logo pull-left">
                            <a href="index.jsp"><img src="images/home/LOGO.png" alt="" /></a>
                        </div>

                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav">
                                 <% if (clt!=null) {%>
                                <li><a href="/myMedicineApp/mescommande.jsp"><i class="fa fa-user"></i> Bonjour <%=clt.getNom()%></a></li>
                                 <%
                                     }
                                  %>
                                <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
                                <li><a href="/myMedicineApp/checkout.jsp"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                                <li><a href="/myMedicineApp/panier.jsp"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                                <% if (clt==null) {%>
                                <li><a href="/myMedicineApp/login.jsp"><i class="fa fa-lock"></i> Login</a></li>
                                <%
                                   }else{
                                %>
                                    <li><a href="/myMedicineApp/Logout"><i class="fa fa-lock"></i> Logout</a></li>
                                <%
                                   }
                                %>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->

        <div class="header-bottom"><!--header-bottom-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                <li><a href="index.jsp" class="active">Home</a></li>
                                <li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
                                        <li><a href="product-details.html">Product Details</a></li> 
                                        <li><a href="checkout.html">Checkout</a></li> 
                                        <li><a href="cart.html">Cart</a></li> 
                                        <li><a href="login.html">Login</a></li> 
                                    </ul>
                                </li> 
                                <% 
                                       
                                        if (role.equals("admin")) {
                                                
                                           %>
                               <li class="dropdown"><a href="#">Product Management<i class="fa fa-angle-down"></i></a>
                                    
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="addProduct.jsp">Add Product</a></li>
                                        <li><a href="listArticle.jsp"> Manage Promotion</a></li> 
                                        <li><a href="listCommande.jsp">Commande List</a></li> 
                                    </ul>
                                </li> 
                                <%
                                   } 
                                %>
                                <li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog.html">Blog List</a></li>
                                        <li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>
                                                              
                                <li><a href="contact-us.html">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="search_box pull-right">
                            <form action="/myMedicineApp/search.jsp">
                                <input type="text" name="mot" placeholder="Search"/>
                                <button type="submit" class="btn btn-outline-dark">Research</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-bottom-->
    </header><!--/header-->
    </body>
</html>
