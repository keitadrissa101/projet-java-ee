<%-- 
    Document   : research
    Created on : 14 déc. 2020, 19:09:23
    Author     : Administrateur
--%>

<%@page import="model.Produit"%>
<%@page import="java.util.List"%>
<%@page import="javax.management.Query"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="controller.Connect"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        <%
            Connection con = Connect.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM categorie");
        %>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Category</h2>
                            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                <% 
                                    while (rs.next()) {
                                %>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a href="#">Respiratory</a></h4>
                                    </div>
                                </div>
                                <%
                                       
                                    }
                                %>
                            </div><!--/category-products-->

                            <div class="brands_products"><!--brands_products-->
                                <h2>Made from </h2>
                                <div class="brands-name">
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="#"> <span class="pull-right">(50)</span>China</a></li>
                                        <li><a href="#"> <span class="pull-right">(56)</span>USA</a></li>
                                        <li><a href="#"> <span class="pull-right">(27)</span>India</a></li>
                                        <li><a href="#"> <span class="pull-right">(32)</span>France</a></li>
                                        <li><a href="#"> <span class="pull-right">(5)</span>Italia</a></li>
                                        <li><a href="#"> <span class="pull-right">(9)</span>Nigeria</a></li>
                                    </ul>
                                </div>
                            </div><!--/brands_products-->

                            <div class="price-range"><!--price-range-->
                                <h2>Price Range</h2>
                                <div class="well text-center">
                                    <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                                    <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                                </div>
                            </div><!--/price-range-->
                            <div class="shipping text-center"><!--shipping-->
                                <img src="images/home/shipping.jpg" alt="" />
                            </div><!--/shipping-->
                        </div>
                    </div>

                    <div class="col-sm-9 padding-right">
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">Features Items</h2>
                            <% 
                                                    String mot = request.getParameter("mot");
                                                    if (mot==null) {
                                                            mot = "";
                                                        }
                                                    String req = "Select * produit where libelle like '%"+mot.toLowerCase()+"%'";
                                                    if (request.getParameter("idc")!=null) {
                                                            int idc = Integer.parseInt(request.getParameter("idc"));
                                                            req+=" and p.categorie.idcat="+idc;
                                                        }
                                                    
                                                   
                                                     rs = st.executeQuery("Select * produit where libelle like '%"+mot.toLowerCase()+"%'");
                                                    
                                                  
                                                    if (rs.next()) {
                                                        %>
                                                        <h2>Not found !!!</h2> 
                                                    <%    }else{
                                                  while (rs.next()) {
                                                %>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="images/home/product1.jpg" alt="" />
                                            <h2>$56</h2>
                                            <p>Antiseptic</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">
                                                <h2>$56</h2>
                                                <p>Antiseptic</p>
                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">
                                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <% 
                                                    }  

                                                      }
                                                    %>
                        </div><!--features_items-->

                        

                    </div>
                </div>
            </div>
        </section>
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
