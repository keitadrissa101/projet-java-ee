<%-- 
    Document   : commande
    Created on : 14 déc. 2020, 18:30:45
    Author     : Administrateur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                      <li><a href="#">Home</a></li>
                      <li class="active">My Orders List</li>
                    </ol>
                </div>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Order ID</td>
                                <td class="description"></td>
                                <td class="price">Date</td>
                                <td class="quantity">State</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="cart_product">
                                    <a href="">50</a>
                                </td>
                                <td class="cart_description">
                                    <a href=""></a>
                                </td>
                                <td class="cart_price">
                                    <a href="">24/11/2020</a>
                                </td>
                                <td class="cart_quantity">
                                    <a>In progess</a>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">$175</p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                    <a class="cart_quantity_delete" href="">Details</i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="cart_product">
                                    <a href="">50</a>
                                </td>
                                <td class="cart_quantity">
                                    
                                </td>
                                <td class="cart_description">
                                    <a href=""> 24/11/2020</a>
                                </td>
                                <td class="cart_price">
                                    <a href="">In progess</a>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">$130</p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                    <a class="cart_quantity_delete" href="">Details</i></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="cart_product">
                                    <a href="">20</a>
                                </td>
                                <td class="cart_quantity">
                                    
                                </td>
                                <td class="cart_description">
                                    <a href="">24/12/2020</a>
                                </td>
                                <td class="cart_price">
                                    <a href="">In progess</a>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">$120</p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                    <a class="cart_quantity_delete" href="">Details</i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
	</section> <!--/#cart_items-->
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
