<%-- 
    Document   : payment
    Created on : 14 déc. 2020, 19:03:16
    Author     : Administrateur
--%>

<%@page import="model.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Check out</li>
                    </ol>
                </div><!--/breadcrums-->

                <div class="step-one">
                    <h2 class="heading">Step2 : Payment</h2>
                </div>
                <div class="register-req">
                    <p> Pay Your Order Securely</p>
                </div><!--/register-req-->

                <div class="shopper-informations">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="shopper-info">
                            <% 
                            if (request.getAttribute("msg")!=null) {                              
                            %>
                            <h3 style="color: red;"><%=request.getAttribute("msg")%></h3>
                            <% }
                                          Client clt = null;
                                       if (session.getAttribute("clt")!=null) {
                                               clt = (Client)session.getAttribute("clt");
                                           }
                                             
                                    %>
                                <p>Carde Information</p>
                                <form action="PayServlet" method="post">
                                    
                                    <input type="text" placeholder="Card ID" name="numcard">
                                    <input type="text" placeholder="Validation Date" name="vdate">
                                    <input type="password" placeholder="Crypto" name="pwd">
                                    
                                    
                                    <input type="hidden" name="idclt" value="<%=clt.getIdc()%>" >
                                    <input type="hidden" name="adresse1" value="<%=request.getParameter("adresse1")%>" >
                                    <input type="hidden" name="adresse2" value="<%=request.getParameter("adresse2")%>" >
                                    <input type="hidden" name="ville" value="<%=request.getParameter("ville")%>">
                                    <input type="hidden" name="pays" value="<%=request.getParameter("pays")%>" >
                                    <input type="hidden" name="codepostale" value="<%=request.getParameter("codepostale")%>">
                                    
                                    <button type="submit" class="btn btn-primary">Paied</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </section> <!--/#cart_items-->
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
