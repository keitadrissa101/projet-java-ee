<%-- 
    Document   : detailProduct
    Created on : 14 déc. 2020, 19:05:20
    Author     : Administrateur
--%>

<%@page import="java.util.List"%>
<%@page import="controller.ImageMgr"%>
<%@page import="model.Image"%>
<%@page import="controller.ProduitMgr"%>
<%@page import="model.Produit"%>
<%@page import="controller.CategoryMgr"%>
<%@page import="model.Categorie"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        <%

            ArrayList<Categorie> lc = new ArrayList();
            CategoryMgr ctg = new CategoryMgr();
            lc = ctg.list();
        %>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Category</h2>
                            <div class="panel-group category-products" id="accordian"><!--category-productsr-->

                                <%
                                    for (Categorie c : lc) {

                                %>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a href="/myMedicineAPP/search.jsp?idc=<%=c.getIdcat()%>&mot="><%=c.getLibelle()%></a></h4>
                                    </div>
                                </div>
                                <%
                                    }
                                %>

                            </div><!--/category-products-->

                        </div>
                    </div>

                    <%
                        int idp = Integer.parseInt(request.getParameter("idp"));
                        
                        ProduitMgr prg = new ProduitMgr();
                        Produit p = prg.research(idp);
                        
                            
                            String cat = "";
                            switch (p.getIdcat()) {
                                case 1:
                                    cat = "Respiratory";
                                    break;
                                case 2:
                                    cat = "Analgesics";
                                    break;
                                case 3:
                                    cat = "Gastrointestinal";
                                    break;
                                case 4:
                                    cat = "Circulatory";
                                    break;
                                case 5:
                                    cat = "Eye/ear";
                                    break;
                                case 6:
                                    cat = "Oral hygienne";
                                    break;
                                case 7:
                                    cat = "Amtiparasitic";
                                    break;
                            }

                            


                    %>

                    <div class="col-sm-9 padding-right">
                        <div class="product-details"><!--product-details-->
                            <div class="col-sm-5">
                                <div class="view-product">
                                    <%
                                    
                                    ImageMgr mrg = new ImageMgr();
                                    Image img = mrg.rechercher(idp);
                                    %>
                                    <img src="<%=img.getUrl()%>" alt="" />
                                    
                                    <h3>ZOOM</h3>
                                </div>
                                

                            </div>
                            <div class="col-sm-7">
                                <div class="product-information"><!--/product-information-->
                                    <form action="/myMedicineApp/ManageCart" method="get">
                                        <img src="images/product-details/new.jpg" class="newarrival" alt="" />
                                        <h2><%=p.getLibelle()%></h2>
                                        <p><%=cat%></p>
                                        <img src="images/product-details/rating.png" alt="" />
                                        <span>
                                            <span>$<%=p.getLibelle()%></span>
                                            <label>Quantity:</label>
                                            <input type="text" name="qte" value="3" />
                                            <button type="submit" class="btn btn-fefault cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                Add to cart
                                            </button>
                                            <input type="hidden" name="idp" value="<%=p.getIdp()%>" />
                                            <input type="hidden" name="action" value="ajouter" />
                                        </span>
                                    </form>
                                    <p><b>Availability:</b><%=p.getQtestck() > 0 ? "Available" : "Unavailable"%> </p>
                                    <p><b>Condition:</b> New</p>
                                    <p><b>Country</b> <%=p.getCountry()%></p>
                                    <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
                                </div><!--/product-information-->
                            </div>
                        </div><!--/product-details-->
                        
                        
                        <%
                            if (request.getParameter("idc")!=null) {
                                    
                               
            int idc = Integer.parseInt(request.getParameter("idc"));
            String qc = "SELECT * FROM `produit` WHERE idcat =" + idc;
            ArrayList<Produit> l1 = new ArrayList();
            ProduitMgr cmg = new ProduitMgr();
            l1 = cmg.listr(qc);
          %>
                        <div class="recommended_items"><!--recommended_items-->
                            <h2 class="title text-center">recommended items</h2>
                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    
                                    <%
                                   int i=0;
                                for (Produit p1 : l1) {


        %>
                                <div class="item <%=(i==0)?" active":""%>">
                                    <div class="col-sm-4">
                                        <h1><span><%=p1.getPrix()%></span></h1>
                                        <p><%=p1.getLibelle()%></p>
                                        <button type="button" class="btn btn-default get">Get it now</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="images/home/girl1.jpg" class="girl img-responsive" alt="" />
				        
                                    </div>
                                </div>
                                <%
                                   i++;
                                    }  }
                                %>
                                     
                                </div>
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>			
                            </div>
                        </div><!--/recommended_items-->
                    </div>
                                
                </div>
            </div>
        </section>
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
