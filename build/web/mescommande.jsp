<%-- 
    Document   : mescommande
    Created on : 1 janv. 2021, 11:56:52
    Author     : Administrateur
--%>

<%@page import="controller.LignecommandeMgr"%>
<%@page import="model.Commande"%>
<%@page import="java.util.ArrayList"%>
<%@page import="controller.CommandeMgr"%>
<%@page import="model.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        
        <jsp:include page="views/header.jsp"></jsp:include> 
        
        <% 
           if (session.getAttribute("clt")=="") {
                   request.setAttribute("msg", "You must login");
                   request.getRequestDispatcher("login.jsp").forward(request, response);
               }
                 Client c = (Client)session.getAttribute("clt");
                 int idc = c.getIdc();
         
                            if (request.getAttribute("mgs")!="") {
                              
                            %>
                            <h3 style="color: red;"><%=request.getAttribute("msg")%></h3>
                            <%   }
           
           CommandeMgr cmgr = new CommandeMgr();
           
           String req = "SELECT * FROM `commande` WHERE idc='"+idc+"' and etat!='Get'";
           
             ArrayList<Commande> cl = new ArrayList();
                                                    
              cl = cmgr.listr(req);

        
        
        %>
        
        
        <section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">My Orders Liste</li>
				</ol>
			</div>
                    
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Num Commande</td>
							<td class="price">Date</td>
							<td class="quantity">State</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
                                        <%
                                            for (Commande cm : cl) {
                                            int id = cm.getIdcmd();
                                            LignecommandeMgr lcg = new LignecommandeMgr();
                                            int total = lcg.montant(id);
                                            
                                                
                                            
                                            %>
					<tbody>
                                           
						<tr>
							<td class="cart_product">
                                                            <%=cm.getIdcmd() %>
							</td>
							<td class="cart_description">
								<h4><a href=""> <%=cm.getDatecmd() %></a></h4>
                                                                
							</td>
							<td class="cart_price">
								<p><%=cm.getEtat() %></p>
							</td>
							<td class="cart_total">
                                                            <p class="cart_total_price">$<%=total %></p>
							</td>
							<td class="cart_delete">
                                                            <a class="cart_quantity_delete" href="/myMedicineApp/detailCommande.jsp?idcmd=<%=cm.getIdcmd()%>"><i class="fa fa-eye"></i></a>
							</td>
						</tr>

						<% } %>
					</tbody>
				</table>
			</div>
                                       
		</div>
	</section> <!--/#cart_items-->
        
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
