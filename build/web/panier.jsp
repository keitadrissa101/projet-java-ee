<%-- 
    Document   : panier
    Created on : 26 nov. 2020, 13:30:03
    Author     : Administrateur
--%>



<%@page import="model.Image"%>
<%@page import="controller.ImageMgr"%>
<%@page import="model.Produit"%>
<%@page import="model.LignePanier"%>
<%@page import="model.Panier"%>
<%@page import="controller.ProduitMgr"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
          <jsp:include page="/views/header.jsp"></jsp:include>
          
          <%
        ProduitMgr s = new ProduitMgr();   
        
        %>
            
          <section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
                    <%
                        if (session.getAttribute("panier")==null) {
                            %>
                            <h2>Your Card is Empty</h2>
                    <%            
                            }else{
              Panier panier = (Panier)session.getAttribute("panier");
              %>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
                                        <%
                                            for (LignePanier lp : panier.getItems()) {
                                                   
                                                Produit p = lp.getP();
                                                ImageMgr mrg = new ImageMgr();
                                                Image img = mrg.rechercher(p.getIdp());
                                            %>
					<tbody>
                                           
						<tr>
							<td class="cart_product">
                                                            <a href=""><img src="<%=img.getUrl()%>" width="50px" height="50px" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""> <%=p.getLibelle() %></a></h4>
                                                                
							</td>
							<td class="cart_price">
								<p><%=p.getPrix() %></p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href="/myMedicineApp/ManageCart?action=addqte&idp=<%=p.getIdp()%>"> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="<%=lp.getQte()%>" autocomplete="off" size="2">
									<a class="cart_quantity_down" href="/myMedicineApp/ManageCart?action=leftqte&idp=<%=p.getIdp()%>"> - </a>
								</div>
							</td>
							<td class="cart_total">
                                                            <p class="cart_total_price"><%=lp.getQte()*p.getPrix()%>$</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="/myMedicineApp/ManageCart?action=supp&idp=<%=p.getIdp()%>"><i class="fa fa-times"></i></a>
							</td>
						</tr>

						<% } %>
					</tbody>
				</table>
			</div>
                                       
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
                                                    <li>Cart Sub Total <span><%=panier.total()%> $</span></li>
							
                                                        <li>Total <span><%=panier.total()%>$</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="/myMedicineApp/checkout.jsp" class="btn btn-primary">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
          <% } %> 
          <jsp:include page="/views/footer.jsp"></jsp:include>

    </body>
</html>
