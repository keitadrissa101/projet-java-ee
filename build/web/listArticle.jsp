<%-- 
    Document   : listArticle
    Created on : 3 janv. 2021, 20:49:41
    Author     : Administrateur
--%>

<%@page import="controller.ProduitMgr"%>
<%@page import="model.Produit"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/views/header.jsp"></jsp:include>
        
        <section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
                        <% if (request.getAttribute("msg")!=null) {
                              
                            %>
                            <h3 style="color: red;"><%=request.getAttribute("msg")%></h3>
                            <%   }%>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							
							<td></td>
						</tr>
					</thead>
                                        <%
                                            ArrayList<Produit> l = new ArrayList();
                                            ProduitMgr cmg = new ProduitMgr();
                                            l = cmg.list();

                                            for (Produit p : l) {
                                            
                                            %>
					<tbody>
                                           
						<tr>
							<td class="cart_product">
                                                            <a href=""><img src="images/cart/produit/product1"  alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""> <%=p.getLibelle() %></a></h4>
                                                                
							</td>
							<td class="cart_price">
								<p><%=p.getPrix() %></p>
							</td>
							<td class="cart_delete">
                                                            
						            <a class="cart_quantity_delete" href="/myMedicineApp/ManageCart?action=supp&idp=<%=p.getIdp()%>"><i class="fa fa-times"></i></a>
                                                             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-idp="<%=p.getIdp()%>"><i class="fa fa-money"></i></button>
							</td>
						</tr>

						<% } %>
					</tbody>
				</table>
			</div>
                                       
		
                      
	</section> <!--/#cart_items-->
        
        </div>
                  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <form action="AddProSer" method="post">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New Promation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        
                          <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Idp :</label>
                            <input type="text" class="form-control" name="idp" id="idp" readonly>
                          </div>
                          <div class="form-group">
                            <label for="message-text" class="col-form-label">Date Start :</label>
                            <input type="date" class="form-control" name="dates" id="taux">
                          </div>
                          <div class="form-group">
                            <label for="message-text" class="col-form-label">Date fin :</label>
                            <input type="date" class="form-control" name="datef" id="taux">
                          </div>
                           <div class="form-group">
                            <label for="message-text" class="col-form-label"> Rate :</label>
                            <input type="number" class="form-control" name="taux" id="taux">
                          </div>
                        
                      </div>
                      <div class="modal-footer"> 
                        <button type="sumit" class="btn btn-primary">Save</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
        <jsp:include page="/views/footer.jsp"></jsp:include>
        <script>
            $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var idp = button.data('idp') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New promotion for the product ' + idp)
  modal.find('#idp').val(idp)
})

        </script>
    </body>
</html>
