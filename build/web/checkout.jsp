<%-- 
    Document   : checkout
    Created on : 14 déc. 2020, 18:21:05
    Author     : Administrateur
--%>

<%@page import="model.Image"%>
<%@page import="controller.ImageMgr"%>
<%@page import="model.Produit"%>
<%@page import="model.LignePanier"%>
<%@page import="model.Panier"%>
<%@page import="model.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        <% 
           if (session.getAttribute("clt")==null) {
                   request.setAttribute("msg", "You must login");
                   request.getRequestDispatcher("login.jsp").forward(request, response);
               }
           Client c = (Client)session.getAttribute("clt");
        
        %>
        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                      <li><a href="#">Home</a></li>
                      <li class="active">Check out</li>
                    </ol>
                </div><!--/breadcrums-->

                <div class="step-one">
                        <h2 class="heading">Step1</h2>
                </div>
                <form action="/myMedicineApp/payment.jsp" method="post">
                <div class="shopper-informations">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="shopper-info">
                                <p>Customer Information</p>
                                
                                    <input type="text" placeholder="Display Name" value="<%=c.getNom()  %>">
                                    <input type="text" placeholder="User Name" value="<%=c.getPrenom()  %>">
                                
                                
                            </div>
                        </div>
                        <div class="col-sm-5 clearfix">
                            <div class="bill-to">
                                <p>Bill To</p>
                                <div class="form-one">
                                    
                                        <input type="text" placeholder="Address 1" name="adresse1" required>
                                        <input type="text" placeholder="Address 2" name="adresse2" required>
                                    
                                </div>
                                <div class="form-two">
                                    
                                    <input type="text" placeholder="Zip / Postal Code" name="codepostale">
                                        <select name="pays">
                                            <option>-- Country --</option>
                                            <option value="China">China</option>
                                            <option value="United States">United States</option>                                           
                                            <option value="UK">UK</option>                                            
                                            <option value="Canada">Canada</option>
                                            <option value="Dubai">Dubai</option>
                                        </select>
                                        <select name="ville">
                                            <option>-- State / Province / Region --</option>
                                            <option value="United States">Changhai</option>
                                            <option value="Bangladesh">Pekin</option>
                                            <option value="UK">Canton</option>
                                            <option value="India">Shenzhen</option>
                                            <option value="Pakistan">Dongggan</option>
                                            <option value="Ucrane">Tianjin</option>
                                            <option value="Canada">Hong Kong</option>
                                            <option value="Canada">Wuhan</option>
                                        </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="order-message">
                                <p>Shipping Order</p>
                                <textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
                                <label><input type="checkbox"> Shipping to bill address</label>
                            </div>	
                        </div>					
                    </div>
                </div>
                <div class="review-payment">
                    <h2>Review & Payment</h2>
                </div>

                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Item</td>
                                <td class="description"></td>
                                <td class="price">Price</td>
                                <td class="quantity">Quantity</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <%            
                            
              Panier panier = (Panier)session.getAttribute("panier");
              
                                            for (LignePanier lp : panier.getItems()) {
                                                    
                                                Produit p = lp.getP();
                                                ImageMgr mrg = new ImageMgr();
                                                Image img = mrg.rechercher(p.getIdp());
                                           
              %>
                            <tr>
                                <td class="cart_product">
                                    <a href=""><img src="<%=img.getUrl()%>" height="50px" width="50px" alt=""></a>
                                </td>
                                <td class="cart_description">
                                        <h4><a href=""><%=p.getLibelle() %></a></h4>
                                </td>
                                <td class="cart_price">
                                        <p>$<%=p.getPrix() %></p>
                                </td>
                                <td class="cart_quantity">
                                    <input class="cart_quantity_input" type="text" name="quantity" value="<%=lp.getQte() %>" autocomplete="off" size="2">
                                </td>
                                <td class="cart_total">
                                        <p class="cart_total_price">$<%=p.getPrix()*lp.getQte() %></p>
                                </td>
                                <td class="cart_delete">
                                        <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                </div>
                <div class="payment-options">
                    <span>
                        <label><input type="checkbox"> Direct Bank Transfer</label>
                    </span>
                    
                    <button type="submit" class="btn btn-primary">Continue</button>
                </div>
            </div>
	</section> <!--/#cart_items-->
        </form>
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
