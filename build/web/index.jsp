<%-- 
    Document   : index
    Created on : 18 déc. 2020, 18:20:04
    Author     : Administrateur
--%>

<%@page import="model.Image"%>
<%@page import="controller.ImageMgr"%>
<%@page import="controller.CategoryMgr"%>
<%@page import="model.Categorie"%>
<%@page import="controller.ProduitMgr"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Produit"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head> 
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>

        <%
            ArrayList<Produit> l1 = new ArrayList();
            ProduitMgr cmg = new ProduitMgr();
            l1 = cmg.list2();
          %>
            
        <section id="slider"><!--slider-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <% for (int i = 0; i <l1.size(); i++) { %>
                                <li data-target="#slider-carousel" data-slide-to="<%=i%>"<%=(i==0)?"class=\"active\"":""%>></li>
                                <%}%>
                            </ol>

                            <div class="carousel-inner">
                               <%
                                   int i=0;
                                for (Produit p : l1) {

                String cat = "";
                switch (p.getIdcat()) {
                    case 1:
                        cat = "Respiratory";
                        break;
                    case 2:
                        cat = "Analgesics";
                        break;
                    case 3:
                        cat = "Gastrointestinal";
                        break;
                    case 4:
                        cat = "Circulatory";
                        break;
                    case 5:
                        cat = "Eye/ear";
                        break;
                    case 6:
                        cat = "Oral hygienne";
                        break;
                    case 7:
                        cat = "Amtiparasitic";
                        break;
                }

                ImageMgr mrg = new ImageMgr();
                Image img = mrg.rechercher(p.getIdp());
        %>
                                <div class="item <%=(i==0)?" active":""%>">
                                    <div class="col-sm-6">
                                        <h1><span><%=p.getLibelle()%></span></h1>
                                        <p><%=p.getDescription()%></p>
                                         <a href="/myMedicineApp/ManageCart?action=ajouter&idp=<%=p.getIdp()%>&qte=1" class="btn btn-default">Get it now</a>
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="<%=img.getUrl()%>" class="girl img-responsive" alt="" />
				        <img src="images/home/pricing.png"  class="pricing" alt="" />
                                    </div>
                                </div>
                                <%
                                   i++;
                                    }
                                %>

                            </div>

                            <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </section><!--/slider-->
        <%
            ArrayList<Categorie> lc = new ArrayList();
            CategoryMgr ctg = new CategoryMgr();
            lc = ctg.list();


        %>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Category</h2>
                            <div class="panel-group category-products" id="accordian"><!--category-productsr-->


                            <% 
                                for (Categorie c : lc) {
                            %>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/myMedicineApp/search.jsp?idc=<%=c.getIdcat()%>&mot="><%=c.getLibelle()%></a></h4>
                                </div>
                            </div>

                            <%                                                             }
                            %>

                            </div><!--/category-products-->

                            <div class="brands_products"><!--brands_products-->
                                <h2>Made from </h2>
                                <div class="brands-name">
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="#"> <span class="pull-right">(50)</span>China</a></li>
                                        <li><a href="#"> <span class="pull-right">(56)</span>USA</a></li>
                                        <li><a href="#"> <span class="pull-right">(27)</span>India</a></li>
                                        <li><a href="#"> <span class="pull-right">(32)</span>France</a></li>
                                        <li><a href="#"> <span class="pull-right">(5)</span>Italia</a></li>
                                        <li><a href="#"> <span class="pull-right">(9)</span>Nigeria</a></li>
                                    </ul>
                                </div>
                            </div><!--/brands_products-->

                            <div class="price-range"><!--price-range-->
                                <h2>Price Range</h2>
                                <div class="well text-center">
                                    <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                                    <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                                </div>
                            </div><!--/price-range-->

                            <div class="shipping text-center"><!--shipping-->
                                <img src="images/home/shipping.jpg" alt="" />
                            </div><!--/shipping-->

                        </div>
                    </div>

                    <div class="col-sm-9 padding-right">
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">Features Items</h2>
                            <%
                                ArrayList<Produit> l2 = new ArrayList();
                                ProduitMgr cmg2 = new ProduitMgr();
                                l2 = cmg2.list2();

                                for (Produit p2 : l2) {

                                    String cat = "";
                                    switch (p2.getIdcat()) {
                                        case 1:
                                            cat = "Respiratory";
                                            break;
                                        case 2:
                                            cat = "Analgesics";
                                            break;
                                        case 3:
                                            cat = "Gastrointestinal";
                                            break;
                                        case 4:
                                            cat = "Circulatory";
                                            break;
                                        case 5:
                                            cat = "Eye/ear";
                                            break;
                                        case 6:
                                            cat = "Oral hygienne";
                                            break;
                                        case 7:
                                            cat = "Amtiparasitic";
                                            break;
                                    }
                                    int idp2 = p2.getIdp();
                                   ImageMgr mrg2 = new ImageMgr();
                                   Image img2 = mrg2.rechercher(idp2);
                            %>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="<%=img2.getUrl()%>" alt="" />
                                            <h2>$<%=p2.getPrix()%></h2>
                                            <p><%=p2.getLibelle()%></p>
                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i><%=img2.getUrl()%></a>
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">
                                                <h2>$<%=p2.getPrix()%></h2>
                                                <p><%=p2.getLibelle()%></p>
                                                <a href="/myMedicineApp/detailProduct.jsp?idp=<%=p2.getIdp()%>&idc=<%=p2.getIdcat()%>" class="btn btn-default add-to-cart"><i class="fa fa-eye"></i>Product details</a>
                                                <a href="/myMedicineApp/ManageCart?action=ajouter&idp=<%=p2.getIdp()%>&qte=1" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">
                                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <%
                                }
                            %>

                        </div><!--features_items-->

                        <div class="category-tab"><!--category-tab-->
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tshirt" data-toggle="tab">Nutritional</a></li>
                                    <li><a href="#blazers" data-toggle="tab">Antiparasitic</a></li>
                                    <li><a href="#sunglass" data-toggle="tab">Gastrointestinal</a></li>
                                    <li><a href="#kids" data-toggle="tab">Circulatory</a></li>
                                    <li><a href="#poloshirt" data-toggle="tab">Respiratory</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tshirt" >
                                    <div class="col-sm-3">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo text-center">
                                                    <img src="images/home/gallery1.jpg" alt="" />
                                                    <h2>$56</h2>
                                                    <p>Ointment</p>
                                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        </div><!--/category-tab-->

                        <div class="recommended_items"><!--recommended_items-->
                            <h2 class="title text-center">recommended items</h2>

                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">	
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">
                                                        <img src="images/home/recommend1.jpg" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Aspirins</p>
                                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>			
                                </div>
                            </div><!--/recommended_items-->

                        </div>
                    </div>
                </div>
        </section>
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
