/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Administrateur
 */
public class Lignecommande {
    private int idlc;
     private int idcm;
     private int idp;
     private int qte;
     private int prixAchat;

    public Lignecommande() {
    }

    public Lignecommande(int idcm, int idp, int qte, int prixAchat) {
        this.idcm = idcm;
        this.idp = idp;
        this.qte = qte;
        this.prixAchat = prixAchat;
    }

    public Lignecommande(int idlc, int idcm, int idp, int qte, int prixAchat) {
        this.idlc = idlc;
        this.idcm = idcm;
        this.idp = idp;
        this.qte = qte;
        this.prixAchat = prixAchat;
    }

    public int getIdlc() {
        return idlc;
    }

    public void setIdlc(int idlc) {
        this.idlc = idlc;
    }

    public int getIdcm() {
        return idcm;
    }

    public void setIdcm(int idcm) {
        this.idcm = idcm;
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public int getPrixAchat() {
        return prixAchat;
    }

    public void setPrixAchat(int prixAchat) {
        this.prixAchat = prixAchat;
    }

         
}
