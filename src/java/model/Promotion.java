/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Administrateur
 */
public class Promotion {
     private int idprom;
     private int idp;
     private Date dateDebut;
     private Date dateFin;
     private Integer taux;

    public Promotion() {
    }

    public Promotion(int idp, Date dateDebut, Date dateFin, Integer taux) {
        this.idp = idp;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.taux = taux;
    }

    public Promotion(int idprom, int idp, Date dateDebut, Date dateFin, Integer taux) {
        this.idprom = idprom;
        this.idp = idp;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.taux = taux;
    }

    public int getIdprom() {
        return idprom;
    }

    public void setIdprom(int idprom) {
        this.idprom = idprom;
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Integer getTaux() {
        return taux;
    }

    public void setTaux(Integer taux) {
        this.taux = taux;
    }

}
