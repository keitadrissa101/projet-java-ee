/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;


/**
 *
 * @author Administrateur
 */
public class Client {
     private int idc;
     private int idcpt;
     private String nom;
     private String prenom;
     private String email;
     private String tel;
     private Date regDate;

    public Client() {
    }

    public Client(int idcpt, String nom, String prenom, String email, String tel, Date regDate) {
        this.idcpt = idcpt;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.tel = tel;
        this.regDate = regDate;
    }

    public Client(int idc, int idcpt, String nom, String prenom, String email, String tel, Date regDate) {
        this.idc = idc;
        this.idcpt = idcpt;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.tel = tel;
        this.regDate = regDate;
    }

    public int getIdc() {
        return idc;
    }

    public void setIdc(int idc) {
        this.idc = idc;
    }

    public int getIdcpt() {
        return idcpt;
    }

    public void setIdcpt(int idcpt) {
        this.idcpt = idcpt;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    
}
