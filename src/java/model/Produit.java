/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Administrateur
 */
public class Produit {
    private int idp;
    private int idcat;
     private String libelle;
     private String country;
     private Integer prix;
     private String description;
     private Integer qtestck;
     private Date datePub;
     

    public Produit() {
    }

    public Produit(int idcat, String libelle, String country, Integer prix, String description, Integer qtestck, Date datePub) {
        this.idcat = idcat;
        this.libelle = libelle;
        this.country = country;
        this.prix = prix;
        this.description = description;
        this.qtestck = qtestck;
        this.datePub = datePub;
    }

    public Produit(int idp, int idcat, String libelle, String country, Integer prix, String description, Integer qtestck, Date datePub) {
        this.idp = idp;
        this.idcat = idcat;
        this.libelle = libelle;
        this.country = country;
        this.prix = prix;
        this.description = description;
        this.qtestck = qtestck;
        this.datePub = datePub;
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public int getIdcat() {
        return idcat;
    }

    public void setIdcat(int idcat) {
        this.idcat = idcat;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQtestck() {
        return qtestck;
    }

    public void setQtestck(Integer qtestck) {
        this.qtestck = qtestck;
    }

    public Date getDatePub() {
        return datePub;
    }

    public void setDatePub(Date datePub) {
        this.datePub = datePub;
    }

      
}
