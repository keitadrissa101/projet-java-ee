/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Administrateur
 */
public class Image {
    private int idimg;
     private int idp;
     private String url;
     private String filename;

    public Image() {
    }

    public Image(int idp, String url, String filename) {
        this.idp = idp;
        this.url = url;
        this.filename = filename;
    }

    public Image(int idimg, int idp, String url, String filename) {
        this.idimg = idimg;
        this.idp = idp;
        this.url = url;
        this.filename = filename;
    }

    public int getIdimg() {
        return idimg;
    }

    public void setIdimg(int idimg) {
        this.idimg = idimg;
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

   
}
