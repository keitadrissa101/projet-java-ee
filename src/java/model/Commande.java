/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Administrateur
 */
public class Commande {
    private int idcmd;
     private int idc;
     private int ida;
     private Date datecmd;
     private String methodePay;
     private String etat;
     private List<Lignecommande> lignecommandes = new ArrayList<Lignecommande>(0);

    public Commande() {
    }

    public Commande(int idc, int ida, Date datecmd, String methodePay, String etat) {
        this.idc = idc;
        this.ida = ida;
        this.datecmd = datecmd;
        this.methodePay = methodePay;
        this.etat = etat;
    }

    public Commande(int idcmd, int idc, int ida, Date datecmd, String methodePay, String etat) {
        this.idcmd = idcmd;
        this.idc = idc;
        this.ida = ida;
        this.datecmd = datecmd;
        this.methodePay = methodePay;
        this.etat = etat;
    }

   

    public int getIdcmd() {
        return idcmd;
    }

    public void setIdcmd(int idcmd) {
        this.idcmd = idcmd;
    }

    public int getIdc() {
        return idc;
    }

    public void setIdc(int idc) {
        this.idc = idc;
    }

    public int getIda() {
        return ida;
    }

    public void setIda(int ida) {
        this.ida = ida;
    }

    

    public Date getDatecmd() {
        return datecmd;
    }

    public void setDatecmd(Date datecmd) {
        this.datecmd = datecmd;
    }

    public String getMethodePay() {
        return methodePay;
    }

    public void setMethodePay(String methodePay) {
        this.methodePay = methodePay;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public List<Lignecommande> getLignecommandes() {
        return lignecommandes;
    }

    public void setLignecommandes(List<Lignecommande> lignecommandes) {
        this.lignecommandes = lignecommandes;
    }
    
    public int total (){
        int tot =0;
        for(Lignecommande lc : lignecommandes){
            tot+= lc.getPrixAchat()*lc.getQte();
        }
        return tot;
    }
    
}
