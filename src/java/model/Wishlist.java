/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;



/**
 *
 * @author Administrateur
 */
public class Wishlist {
    private int idw;
     private int idc;
     private String libelle;

    public Wishlist(int idc, String libelle) {
        this.idc = idc;
        this.libelle = libelle;
    }

    public Wishlist(int idw, int idc, String libelle) {
        this.idw = idw;
        this.idc = idc;
        this.libelle = libelle;
    }

    public int getIdw() {
        return idw;
    }

    public void setIdw(int idw) {
        this.idw = idw;
    }

    public int getIdc() {
        return idc;
    }

    public void setIdc(int idc) {
        this.idc = idc;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
     

        
}
