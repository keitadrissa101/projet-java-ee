/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Administrateur
 */
public class Commentaire {
     private int idcomm;
     private int idp;
     private int idcmd;
     private String commentaire;
     private Date datePub;

    public Commentaire() {
    }

    public Commentaire(int idp, int idcmd, String commentaire, Date datePub) {
        this.idp = idp;
        this.idcmd = idcmd;
        this.commentaire = commentaire;
        this.datePub = datePub;
    }

    public Commentaire(int idcomm, int idp, int idcmd, String commentaire, Date datePub) {
        this.idcomm = idcomm;
        this.idp = idp;
        this.idcmd = idcmd;
        this.commentaire = commentaire;
        this.datePub = datePub;
    }

    public int getIdcomm() {
        return idcomm;
    }

    public void setIdcomm(int idcomm) {
        this.idcomm = idcomm;
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public int getIdcmd() {
        return idcmd;
    }

    public void setIdcmd(int idcmd) {
        this.idcmd = idcmd;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Date getDatePub() {
        return datePub;
    }

    public void setDatePub(Date datePub) {
        this.datePub = datePub;
    }

        
     
}
