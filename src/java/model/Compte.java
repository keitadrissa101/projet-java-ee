/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Administrateur
 */
public class Compte {
    private int idc;
    private String login;
     private String mdp;
     private String role;
     private String question;
     private String response;
     private Date dateCreation;

    public Compte() {
    }

    public Compte(String login, String mdp, String role, String question, String response, Date dateCreation) {
        this.login = login;
        this.mdp = mdp;
        this.role = role;
        this.question = question;
        this.response = response;
        this.dateCreation = dateCreation;
    }

    public Compte(int idc, String login, String mdp, String role, String question, String response, Date dateCreation) {
        this.idc = idc;
        this.login = login;
        this.mdp = mdp;
        this.role = role;
        this.question = question;
        this.response = response;
        this.dateCreation = dateCreation;
    }

    public int getIdc() {
        return idc;
    }

    public void setIdc(int idc) {
        this.idc = idc;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
     
     
}
