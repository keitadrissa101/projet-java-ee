/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Administrateur
 */
public class LignePanier {
    private Produit p;
    private int qte;

    public LignePanier() {
    }
   
    
    public LignePanier(Produit p, int qte) {
        this.p = p;
        this.qte = qte;
    }
    
    

    public Produit getP() {
        return p;
    }

    public void setP(Produit p) {
        this.p = p;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    
    
}
