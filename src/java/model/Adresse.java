/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Administrateur
 */
public class Adresse {
     private int ida;
     private String ad;
     private String ville;
     private int codepostale;
     private String pays;
     private int idc;

    public Adresse() {
    }

    public Adresse(String ad, String ville, int codepostale, String pays, int idc) {
        this.ad = ad;
        this.ville = ville;
        this.codepostale = codepostale;
        this.pays = pays;
        this.idc = idc;
    }

    public Adresse(int ida, String ad, String ville, int codepostale, String pays, int idc) {
        this.ida = ida;
        this.ad = ad;
        this.ville = ville;
        this.codepostale = codepostale;
        this.pays = pays;
        this.idc = idc;
    }

    public int getIda() {
        return ida;
    }

    public void setIda(int ida) {
        this.ida = ida;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getCodepostale() {
        return codepostale;
    }

    public void setCodepostale(int codepostale) {
        this.codepostale = codepostale;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public int getIdc() {
        return idc;
    }

    public void setIdc(int idc) {
        this.idc = idc;
    }

    
}
