/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author Administrateur
 */
public class Expedition {
    private int idex;
    private int idcmd;
     private String moyenExpedition;
     private String numeroExpedition;
     private String etat;

    public Expedition() {
    }

    public Expedition(int idcmd, String moyenExpedition, String numeroExpedition, String etat) {
        this.idcmd = idcmd;
        this.moyenExpedition = moyenExpedition;
        this.numeroExpedition = numeroExpedition;
        this.etat = etat;
    }

    public Expedition(int idex, int idcmd, String moyenExpedition, String numeroExpedition, String etat) {
        this.idex = idex;
        this.idcmd = idcmd;
        this.moyenExpedition = moyenExpedition;
        this.numeroExpedition = numeroExpedition;
        this.etat = etat;
    }

    

    public int getIdex() {
        return idex;
    }

    public void setIdex(int idex) {
        this.idex = idex;
    }

    public String getMoyenExpedition() {
        return moyenExpedition;
    }

    public void setMoyenExpedition(String moyenExpedition) {
        this.moyenExpedition = moyenExpedition;
    }

    public String getNumeroExpedition() {
        return numeroExpedition;
    }

    public void setNumeroExpedition(String numeroExpedition) {
        this.numeroExpedition = numeroExpedition;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public int getIdcmd() {
        return idcmd;
    }

    public void setIdcmd(int idcmd) {
        this.idcmd = idcmd;
    }

    
     
}
