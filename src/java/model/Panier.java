/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ProduitMgr;
import java.util.ArrayList;

/**
 *
 * @author Administrateur
 */
public class Panier {
    
    private ArrayList<LignePanier> items  = new ArrayList<LignePanier>();

    public Panier() {
//        Produit p = new Produit(40, "Aspirine", "China", 20, "good for helth", 10, new Date());
//        LignePanier lp = new LignePanier(p, 5);
//        items.add(lp);
    }
    

    public ArrayList<LignePanier> getItems() {
        return items;
    }

    public void setItems(ArrayList<LignePanier> items) {
        this.items = items;
    }
    
    public void addItems(int idp, int qte) {
        boolean flag = true;
        for (LignePanier lp : items) {
            if (lp.getP().getIdp()==idp) {
                lp.setQte(lp.getQte()+qte);
                flag = false;
                break;
            }
            
        }
        
        if (flag) {
                //Session s = MyHibernateUtil.getSession();
                ProduitMgr pm = new ProduitMgr();
                Produit pro =  pm.research(idp);
                LignePanier lipa = new LignePanier(pro, qte);
                items.add(lipa);
                
            }
    }

    public void addQte(int idp) {
       for (LignePanier lp : items) {
            if (lp.getP().getIdp()==idp) {
                lp.setQte(lp.getQte()+1);
                
                break;
            }
    }
    }
    
    public void leftQte(int idp) {
       for (LignePanier lp : items) {
            if (lp.getP().getIdp()==idp) {
                lp.setQte(lp.getQte()-1);
                if (lp.getQte()==0) {
                   items.remove(lp);
                }
                break;
            }
    }
    }
    
    
    
    public int total() {
        int f = 0;
        for (LignePanier lp : items) {
            f+= lp.getP().getPrix()*lp.getQte();
                
    }
        return f;
    }

    public void supprimerPro(int idp) {
        for (LignePanier lp : items) {
            if (lp.getP().getIdp()==idp) {
                items.remove(lp);
                
                break;
            }
    }
    }
}
