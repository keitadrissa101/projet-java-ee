/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import BD.Connect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrateur
 */
public class AddPromotion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddPromotion</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddPromotion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Statement st;
        Connection con;
        PrintWriter out = response.getWriter();
        HttpSession session;
        SimpleDateFormat amj = new SimpleDateFormat("dd/MM/yyyy");
         int id = Integer.parseInt(request.getParameter("idp"));
            String dd = amj.format(request.getParameter("dated"));
            String df = amj.format(request.getParameter("datef"));
            int t = Integer.parseInt(request.getParameter("taux"));
             out.println(id+", "+dd+", "+df+", "+t);
//        try {
           
//        con = Connect.getConnection();
//        st = con.createStatement();
//        String sq="INSERT INTO `promotion`(`idp`, `date_debut`, `date_fin`, `taux`) VALUES ('"+id+"','"+dd+"' ,'"+df+"','"+t+"')";        
//        st.executeUpdate(sq);
//        st.close();
//        con.close();
//        st=null;
//        con=null;
        
//        request.setAttribute("msg", "Promotion add susscefully");
//        request.getRequestDispatcher("listArticle.jsp").forward(request, response);
//        } catch (Exception e) {
//        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
