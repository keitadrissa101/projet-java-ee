/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import BD.Connect;
import controller.AdresseMgr;
import controller.CommandeMgr;
import controller.LignecommandeMgr;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Adresse;
import model.Commande;
import model.LignePanier;
import model.Lignecommande;
import model.Panier;

/**
 *
 * @author Administrateur
 */
public class PayServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PayServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PayServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Statement st;
        Connection con;
        
        HttpSession session;
            
           try {
               
                PrintWriter out = response.getWriter();
                String num = request.getParameter("numcard");
                String vd = request.getParameter("vdate");
                String mdp = request.getParameter("pwd");
        
               
            if (num.equals("123123") && vd.equals("12/21") && mdp.equals("123")) {
            String ad1 = request.getParameter("adresse1");
            String ad2 = request.getParameter("adresse2");
            String py = request.getParameter("pays");
            String v = request.getParameter("ville");
            int cp = Integer.parseInt(request.getParameter("codepostale"));
            int idclt = Integer.parseInt(request.getParameter("idclt"));
//           out.println(ad1+", "+py+", "+v+", "+cp+", "+idclt+", "+ad2);
            con = Connect.getConnection();
            st = con.createStatement();
                String sql="INSERT INTO `adresse`(`ad`, `ville`, `codepostale`, `pays`, `idc`) VALUES ('"+ad2+"','"+v+"','"+cp+"','"+py+"','"+idclt+"')";
                st.executeUpdate(sql);
                st.close();
                con.close();
                st=null;
                con=null;
                AdresseMgr admg = new AdresseMgr();
                int lad = admg.lastid();
                
                CommandeMgr cmgr = new CommandeMgr();
                Commande cd = new Commande(idclt, lad, new Date(), "VISA", "en progress...");
             
                cmgr.add(cd);
                
                int idcd = cmgr.lastid();
                session = request.getSession();
                Panier pan = (Panier)session.getAttribute("panier");
                
                 
                for (LignePanier lp : pan.getItems()) {
                    con = Connect.getConnection();
                    st = con.createStatement();
                    Lignecommande lcd = new Lignecommande(idcd, lp.getP().getIdp(), lp.getQte(), lp.getP().getPrix());
                    cd.getLignecommandes().add(lcd);
                    out.println( lp.getP().getIdp());
                    String sql2="INSERT INTO `lignecommande`(`idcmd`, `idp`, `qte`, `prix_achat`) VALUES ('"+idcd+"','"+lp.getP().getIdp()+"','"+lp.getQte()+"','"+lp.getP().getPrix()+"')";
                    st.executeUpdate(sql2);
                    st.close();
                con.close();
                st=null;
                con=null;
                }
                
                
                session.removeAttribute("panier");
                
                request.setAttribute("mgs", "Your order is done successfuly it is now in process to expedition");
                request.getRequestDispatcher("mescommande.jsp").forward(request, response);
            
                
                 
                 
                 } else {
            request.setAttribute("mgs", "Incorrect Information");
            request.getRequestDispatcher("payment.jsp").forward(request, response);
        }
                 
        } catch (Exception e) {
        }
          
            
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
