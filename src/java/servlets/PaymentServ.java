/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controller.AdresseMgr;
import controller.CommandeMgr;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Adresse;
import model.Commande;
import model.LignePanier;
import model.Lignecommande;
import model.Panier;

/**
 *
 * @author Administrateur
 */
public class PaymentServ extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PaymentServ</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PaymentServ at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        String num = request.getParameter("numcard");
        String vd = request.getParameter("vdate");
        String mdp = request.getParameter("pwd");
        out.println(num+", "+vd+", "+mdp);
        
        if (num.equals("123123")&&vd.equals("12/21")&mdp.equals("123")) {
            String ad1 = request.getParameter("adresse1");
            String ad2 = request.getParameter("adresse2");
            String py = request.getParameter("pays");
            String v = request.getParameter("ville");
            String cp = request.getParameter("codepostale");
            int idclt = Integer.parseInt(request.getParameter("idclt"));
            
            out.println(ad2+", "+cp+", "+v);
            
           
            
            AdresseMgr adm = new AdresseMgr();
            Adresse ad = new Adresse(ad2, v, Integer.parseInt(cp), py, idclt);
            adm.add(ad);
             
            int lad = adm.lastid();
            CommandeMgr cmgr = new CommandeMgr();
            Commande cd = new Commande(idclt, lad, 1, new Date(), "VISA", "en cours...");
            cmgr.add(cd);
            
            int idcd = cmgr.lastid();
            Panier pan = (Panier)request.getSession().getAttribute("panier");
           
                for (LignePanier lp : pan.getItems()) {
                    Lignecommande lcd = new Lignecommande(idcd, lp.getP().getIdp(), lp.getQte(), lp.getP().getPrix());
                    cd.getLignecommandes().add(lcd);
                }
                
           
            
            
            request.getSession().removeAttribute("panier");
            request.setAttribute("mgs", "Your order is done successfuly it is now in process to expedition");
            request.getRequestDispatcher("mescommande.jsp").forward(request, response);

            
        } else {
            request.setAttribute("mgs", "Incorrect Information");
            request.getRequestDispatcher("payment.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
