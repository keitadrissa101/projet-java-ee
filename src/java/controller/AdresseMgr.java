/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Adresse;

/**
 *
 * @author Administrateur
 */
public class AdresseMgr {

    public AdresseMgr() {
    }
    
    public void add(Adresse a){
        
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `adresse`(`ad`, `ville`, `codepostale`, `pays`, `idc`) VALUES (?,?,?,?,?)");
           
            prepare.setString(1, a.getAd());
            prepare.setString(2, a.getVille());
            prepare.setString(3, a.getPays());
            prepare.setInt(4, a.getCodepostale());
            prepare.setInt(5, a.getIdc());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Adresse a, String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `ad` SET `ad`=?,`ville`=?,`codepostale`=?,`pays`=?,`idc`=? WHERE ida"+id);
            
            prepare.setString(1, a.getAd());
            prepare.setString(2, a.getVille());
            prepare.setString(3, a.getPays());
            prepare.setString(4, String.valueOf(a.getCodepostale()));
            prepare.setInt(5, a.getIdc());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM adresse WHERE ida ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Adresse> list(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Adresse obj = new Adresse();
            
             rs = con.createStatement().executeQuery("SELECT * FROM adresse ");
            while(rs.next()){
            obj.setIda(rs.getInt("ida"));
            obj.setAd(rs.getString("ad"));
            obj.setVille(rs.getString("ville"));
            obj.setPays(rs.getString("pays"));
            obj.setCodepostale(rs.getInt("codepostale"));
            obj.setIdc(rs.getInt("idc"));
            liste.add(obj);
            obj = new Adresse();   
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public int lastid(){
        int max = 0;
    
     ResultSet result = null;
         try {
              Connection con = Connect.getConnection();
             result = con.createStatement().executeQuery("SELECT MAX(ida) as maximum FROM adresse");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
         } catch (Exception e) {
         }
     
        
        return max;
    }
}
