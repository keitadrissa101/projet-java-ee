/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Commande;

/**
 *
 * @author Administrateur
 */
public class CommandeDao extends Dao <Commande> {

    @Override
    public Commande rechercher(long id) {
        ResultSet result = null;
        Commande c = new Commande();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM commande WHERE idcmd ="+id);
            if(result.next() ){
                c.setIdcmd(result.getInt("idcmd"));
                c.setIdc(result.getInt("idc"));
                c.setIda(result.getInt("ida"));
                c.setDatecmd(result.getDate("datecmd"));
                c.setMethodePay(result.getString("methode_pay"));
                c.setEtat(result.getString("etat"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return c;
    }

    @Override
    public void inserer(Commande c) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `commande`( `idc`, `ida`, `idexp`, `datecmd`, `methode_pay`, `etat`) VALUES (?,?,?,?,?,?)");
            prepare.setString(1, String.valueOf(c.getIdc()));
            prepare.setString(2, String.valueOf(c.getIda()));
            prepare.setString(4, amj.format(c.getDatecmd()));
            prepare.setString(5, c.getMethodePay());
            prepare.setString(6, c.getEtat());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Commande c, long id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
         try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `commande` SET `idc`=?,`ida`=?,`idexp`=?,`datecmd`=?,`methode_pay`=?,`etat`=? WHERE idcmd"+id);
            prepare.setString(1, String.valueOf(c.getIdc()));
            prepare.setString(2, String.valueOf(c.getIda()));
            prepare.setString(4, amj.format(c.getDatecmd()));
            prepare.setString(5, c.getMethodePay());
            prepare.setString(6, c.getEtat());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM commande WHERE idcmd ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Commande> liste() {
        List<Commande> Commande = new ArrayList();
        Commande obj = new Commande();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM commande ");
            while(result.next()){
            obj.setIdcmd(result.getInt("idcmd"));
            obj.setIdc(result.getInt("idc"));
            obj.setIda(result.getInt("ida"));
            obj.setDatecmd(result.getDate("datecomd"));
            obj.setMethodePay(result.getString("methode_pay"));
            obj.setEtat(result.getString("etat"));
            Commande.add(obj);
            obj = new Commande();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Commande;
    }
    
}
