/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Lignecommande;

/**
 *
 * @author Administrateur
 */
public class LignecommandeMgr {

    public LignecommandeMgr() {
    }
    
        public void add(Lignecommande lc){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `lignecommande`(`idcmd`, `idp`, `qte`, `prix_achat`) VALUES (?,?,?,?)");
            prepare.setString(1, String.valueOf(lc.getIdlc()));
            prepare.setString(2, String.valueOf(lc.getIdcm()));
            prepare.setString(3, String.valueOf(lc.getIdp()));
            prepare.setString(4, String.valueOf(lc.getQte()));
            prepare.setString(5, String.valueOf(lc.getPrixAchat()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Lignecommande lc, String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `lignecommande` SET `idcmd`=?,`idp`=?,`qte`=?,`prix_achat`=? WHERE idLC"+id);
            prepare.setString(1, String.valueOf(lc.getIdlc()));
            prepare.setString(2, String.valueOf(lc.getIdcm()));
            prepare.setString(3, String.valueOf(lc.getIdp()));
            prepare.setString(4, String.valueOf(lc.getQte()));
            prepare.setString(5, String.valueOf(lc.getPrixAchat()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM lignecommande WHERE idlc ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Lignecommande> list(){
        Connection con = null;
        ResultSet result = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Lignecommande obj = new Lignecommande();
            
            result = con.createStatement().executeQuery("SELECT * FROM lignecommande ");
            while(result.next()){
            obj.setIdlc(result.getInt("idlc"));
            obj.setIdcm(result.getInt("idcmd"));
            obj.setIdp(result.getInt("idp"));
            obj.setQte(result.getInt("qte"));
            obj.setPrixAchat(result.getInt("prix_achat"));
                
                liste.add(obj);
                obj = new Lignecommande();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    
    public ArrayList<Lignecommande> listr(int id){
        Connection con = null;
        ResultSet result = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Lignecommande obj = new Lignecommande();
            
            result = con.createStatement().executeQuery("SELECT * FROM `lignecommande` WHERE idcmd="+id);
            while(result.next()){
            obj.setIdlc(result.getInt("idlc"));
            obj.setIdcm(result.getInt("idcmd"));
            obj.setIdp(result.getInt("idp"));
            obj.setQte(result.getInt("qte"));
            obj.setPrixAchat(result.getInt("prix_achat"));
                
                liste.add(obj);
                obj = new Lignecommande();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    
    public int montant(int idcmd){
        int montant = 0;
        ResultSet result = null;
        try {
            Connection con = Connect.getConnection();
            result = con.createStatement().executeQuery("SELECT SUM(qte*prix_achat) as montant FROM lignecommande where idcmd="+idcmd);
            if(result.next() ){
              montant = result.getInt("montant");
                
            }
        } catch (Exception e) {
        }
         
        return montant;
    }
}
