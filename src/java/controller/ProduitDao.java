/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Produit;

/**
 *
 * @author Administrateur
 */
public class ProduitDao extends Dao <Produit> {

    @Override
    public Produit rechercher(long id) {
        ResultSet result = null;
        Produit p = new Produit();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM produit WHERE idp ="+id);
            if(result.next() ){
                p.setIdp(result.getInt("idp"));
                p.setIdcat(result.getInt("idcat"));
                p.setLibelle(result.getString("libelle"));
                p.setCountry(result.getString("marque"));
                p.setPrix(result.getInt("prix"));
                p.setDescription(result.getString("description"));
                p.setQtestck(result.getInt("qtestck"));
                p.setDatePub(result.getDate("date_pub"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return p;
    }

    @Override
    public void inserer(Produit p) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `produit`(`idcat`, `libelle`, `marque`, `prix`, `description`, `fraisExpedition`, `disponibilite`, `qtestck`, `date_pub`) VALUES (?,?,?,?,?,?,?,?,?)");
            prepare.setString(1, String.valueOf(p.getIdcat()));
            prepare.setString(2, p.getLibelle());
            prepare.setString(3, p.getCountry());
            prepare.setInt(4, p.getPrix());
            prepare.setString(5, p.getDescription());
            prepare.setInt(8, p.getQtestck());
            prepare.setString(9, amj.format(p.getDatePub()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Produit p, long id) {
         SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
         try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `produit` SET `idcat`=?,`libelle`=?,`marque`=?,`prix`=?,`description`=?,`fraisExpedition`=?,`disponibilite`=?,`qtestck`=?,`date_pub`=? WHERE idp"+id);
            prepare.setString(1, String.valueOf(p.getIdcat()));
            prepare.setString(2, p.getLibelle());
            prepare.setString(3, p.getCountry());
            prepare.setInt(4, p.getPrix());
            prepare.setString(5, p.getDescription());
            prepare.setInt(8, p.getQtestck());
            prepare.setString(9, amj.format(p.getDatePub()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM produit WHERE idp ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Produit> liste() {
        List<Produit> Produit = new ArrayList();
        Produit obj = new Produit();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM produit ");
            while(result.next()){
            obj.setIdp(result.getInt("idp"));
            obj.setIdcat(result.getInt("idcat"));
            obj.setLibelle(result.getString("libelle"));
            obj.setCountry(result.getString("marque"));
            obj.setPrix(result.getInt("prix"));
            obj.setDescription(result.getString("description"));
            obj.setQtestck(result.getInt("qtestck"));
            obj.setDatePub(result.getDate("date_pub"));
            Produit.add(obj);
            obj = new Produit();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Produit;
    }
    
    
    public List<Produit> listePP() {
        List<Produit> Produit = new ArrayList();
        Produit obj = new Produit();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM produit inner join promotion on produit.idp=promotion.idp where "+ new Date()+" between promotion.date_debut and promotion.date_fin");
            while(result.next()){
            obj.setIdp(result.getInt("idp"));
            obj.setIdcat(result.getInt("idcat"));
            obj.setLibelle(result.getString("libelle"));
            obj.setCountry(result.getString("marque"));
            obj.setPrix(result.getInt("prix"));
            obj.setDescription(result.getString("description"));
            obj.setQtestck(result.getInt("qtestck"));
            obj.setDatePub(result.getDate("date_pub"));
            Produit.add(obj);
            obj = new Produit();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Produit;
    }
    
    
    public int lastid(){
        int max = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT MAX(idp) as maximum FROM produit");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CompteDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return max;
    }
    
}
