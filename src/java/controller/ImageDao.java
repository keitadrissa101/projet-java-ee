/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Image;

/**
 *
 * @author Administrateur
 */
public class ImageDao extends Dao <Image> {

    @Override
    public Image rechercher(long id) {
        ResultSet result = null;
        Image i = new Image();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM image WHERE idimg ="+id);
            if(result.next() ){
                i.setIdimg(result.getInt("idimg"));
                i.setIdp(result.getInt("idp"));
                i.setUrl(result.getString("url"));
                i.setFilename(result.getString("filename"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImageDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return i;
    }

    @Override
    public void inserer(Image i) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `image`(`idp`, `url`, `filename`) VALUES (?,?,?)");
            prepare.setInt(1, i.getIdp());
            prepare.setString(2, i.getUrl());
            prepare.setString(3, i.getFilename());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ImageDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Image i, long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `image` SET `idp`=?,`url`=?,`filename`=? WHERE idimg"+id);
            prepare.setInt(1, i.getIdimg());
            prepare.setInt(2, i.getIdp());
            prepare.setString(3, i.getUrl());
            prepare.setString(4, i.getFilename());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ImageDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM image WHERE idimg ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ImageDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Image> liste() {
        List<Image> Image = new ArrayList();
        Image obj = new Image();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM image ");
            while(result.next()){
            obj.setIdimg(result.getInt("idimg"));
            obj.setIdp(result.getInt("idp"));
            obj.setUrl(result.getString("url"));
            obj.setFilename(result.getString("filename"));
            Image.add(obj);
            obj = new Image();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ImageDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Image;
    }
    
}
