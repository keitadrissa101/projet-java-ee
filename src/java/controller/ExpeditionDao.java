/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Expedition;

/**
 *
 * @author Administrateur
 */
public class ExpeditionDao extends Dao <Expedition> {

    @Override
    public Expedition rechercher(long id) {
        ResultSet result = null;
        Expedition e = new Expedition();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM expedition WHERE idex ="+id);
            if(result.next() ){
                e.setIdex(result.getInt("idex"));
                e.setMoyenExpedition(result.getString("moyen_expedition"));
                e.setNumeroExpedition(result.getString("numero_expedition"));
                e.setEtat(result.getString("etat"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ExpeditionDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return e;
    }

    @Override
    public void inserer(Expedition e) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `expedition`(`moyen_expedition`, `numero_expedition`, `etat`) VALUES (?,?,?)");
            prepare.setString(1, String.valueOf(e.getIdex()));
            prepare.setString(2, e.getMoyenExpedition());
            prepare.setString(3, e.getNumeroExpedition());
            prepare.setString(4, e.getEtat());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ExpeditionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Expedition e, long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `expedition` SET `moyen_expedition`=?,`numero_expedition`=?,`etat`=? WHERE idex"+id);
            prepare.setString(1, String.valueOf(e.getIdex()));
            prepare.setString(2, e.getMoyenExpedition());
            prepare.setString(3, e.getNumeroExpedition());
            prepare.setString(4, e.getEtat());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ExpeditionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM expedition WHERE idex ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ExpeditionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Expedition> liste() {
       List<Expedition> Expedition = new ArrayList();
        Expedition obj = new Expedition();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM expedition ");
            while(result.next()){
            obj.setIdex(result.getInt("idex"));
            obj.setMoyenExpedition(result.getString("moyen_expedition"));
            obj.setNumeroExpedition(result.getString("numero_expedition"));
            obj.setEtat(result.getString("etat"));
            Expedition.add(obj);
            obj = new Expedition();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ExpeditionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Expedition; 
    }
    
}
