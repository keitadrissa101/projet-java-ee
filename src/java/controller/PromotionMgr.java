/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Promotion;

/**
 *
 * @author Administrateur
 */
public class PromotionMgr {

    public PromotionMgr() {
    }
    
    public void add(Promotion p){
            SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `promotion`(`idp`, `date_debut`, `date_fin`, `taux`) VALUES (?,?,?,?)");
            prepare.setInt(1, p.getIdp());
            prepare.setString(2, amj.format(p.getDateDebut()));
            prepare.setString(3, amj.format(p.getDateFin()));;
            prepare.setInt(4, p.getTaux());
            prepare.executeUpdate();

            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Promotion p, String id){
         SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `promotion` SET `idp`=?,`date_debut`=?,`date_fin`=?,`taux`=? WHERE idprom="+id);
            prepare.setInt(1, p.getIdp());
            prepare.setString(2, amj.format(p.getDateDebut()));
            prepare.setString(3, amj.format(p.getDateFin()));;
            prepare.setInt(4, p.getTaux());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM promotion WHERE idprom ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Promotion> list(){
        Connection con = null;
        ResultSet result = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Promotion obj = new Promotion();
            
            result = con.createStatement().executeQuery("SELECT * FROM `promotion`");
            while(result.next()){
            obj.setIdprom(result.getInt("idprom"));
            obj.setIdp(result.getInt("idp"));
            obj.setDateDebut(result.getDate("date_debut"));
            obj.setDateFin(result.getDate("date_fin"));
            obj.setTaux(result.getInt("taux"));
                
                liste.add(obj);
                obj = new Promotion();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
}
