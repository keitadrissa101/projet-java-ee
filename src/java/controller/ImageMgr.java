/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Image;

/**
 *
 * @author Administrateur
 */
public class ImageMgr {

    public ImageMgr() {
    }
    
        public Image rechercher(long id) {
        ResultSet result = null;
        Image i = new Image();
            try {
                Connection con = Connect.getConnection();
                result = con.createStatement().executeQuery("SELECT * FROM `image` WHERE idp ="+id);
            if(result.next() ){
                i.setIdimg(result.getInt("idimg"));
                i.setIdp(result.getInt("idp"));
                i.setUrl(result.getString("url"));
                i.setFilename(result.getString("filename"));
                
                
            }
            } catch (Exception e) {
            }
        
        return i;
    }
        
        
        public void add(Image i){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `image`(`idp`, `url`, `filename`) VALUES (?,?,?)");
            prepare.setInt(1, i.getIdp());
            prepare.setString(2, i.getUrl());
            prepare.setString(3, i.getFilename());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Image i, String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `image` SET `idp`=?,`url`=?,`filename`=? WHERE idimg"+id);
            prepare.setInt(1, i.getIdimg());
            prepare.setInt(2, i.getIdp());
            prepare.setString(3, i.getUrl());
            prepare.setString(4, i.getFilename());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM image WHERE idimg ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Image> list(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Image obj = new Image();
            
             rs = con.createStatement().executeQuery("SELECT * FROM image ");
            while(rs.next()){
            obj.setIdimg(rs.getInt("idimg"));
            obj.setIdp(rs.getInt("idp"));
            obj.setUrl(rs.getString("url"));
            obj.setFilename(rs.getString("filename"));
                
                liste.add(obj);
                obj = new Image();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public ArrayList<Image> listr(String sql){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Image obj = new Image();
            
             rs = con.createStatement().executeQuery(sql);
            while(rs.next()){
            obj.setIdimg(rs.getInt("idimg"));
            obj.setIdp(rs.getInt("idp"));
            obj.setUrl(rs.getString("url"));
            obj.setFilename(rs.getString("filename"));
                
                liste.add(obj);
                obj = new Image();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
}
