/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Commande;

/**
 *
 * @author Administrateur
 */
public class CommandeMgr {

    public CommandeMgr() {
    }
    
    
    public Commande rechercher(int id) {
        ResultSet result = null;
        Commande c = new Commande();
        
        try {
            Connection con = Connect.getConnection();
            result = con.createStatement().executeQuery("SELECT * FROM commande WHERE idcmd="+id);
            if(result.next() ){
                c.setIdcmd(result.getInt("idcmd"));
                c.setIdc(result.getInt("idc"));
                c.setIda(result.getInt("ida"));
                c.setDatecmd(result.getDate("datecmd"));
                c.setMethodePay(result.getString("methode_pay"));
                c.setEtat(result.getString("etat"));
                
                
            }
        } catch (Exception e) {
        }
        
        
        return c;
    }
    
        public void add(Commande c){
            SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `commande`( `idc`, `ida`, `datecmd`, `methode_pay`, `etat`) VALUES (?,?,?,?,?)");
            prepare.setInt(1, c.getIdc());
            prepare.setInt(2, c.getIda());
            prepare.setString(3, amj.format(c.getDatecmd()));
            prepare.setString(4, c.getMethodePay());
            prepare.setString(5, c.getEtat());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Commande c, String id){
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `commande` SET `idc`=?,`ida`=?,`datecmd`=?,`methode_pay`=?,`etat`=? WHERE idcmd"+id);
            prepare.setString(1, String.valueOf(c.getIdc()));
            prepare.setString(2, String.valueOf(c.getIda()));
            prepare.setString(3, amj.format(c.getDatecmd()));
            prepare.setString(4, c.getMethodePay());
            prepare.setString(5, c.getEtat());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM commande WHERE idcmd ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Commande> list(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Commande obj = new Commande();
            
             rs = con.createStatement().executeQuery("SELECT * FROM commande ");
            while(rs.next()){
            obj.setIdcmd(rs.getInt("idcmd"));
            obj.setIdc(rs.getInt("idc"));
            obj.setIda(rs.getInt("ida"));
            obj.setDatecmd(rs.getDate("datecomd"));
            obj.setMethodePay(rs.getString("methode_pay"));
            obj.setEtat(rs.getString("etat"));
            
                liste.add(obj);
                obj = new Commande();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public ArrayList<Commande> listr(String sql){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Commande obj = new Commande();
            
             rs = con.createStatement().executeQuery(sql);
            while(rs.next()){
            obj.setIdcmd(rs.getInt("idcmd"));
            obj.setIdc(rs.getInt("idc"));
            obj.setIda(rs.getInt("ida"));
            obj.setDatecmd(rs.getDate("datecmd"));
            obj.setMethodePay(rs.getString("methode_pay"));
            obj.setEtat(rs.getString("etat"));
            
                liste.add(obj);
                obj = new Commande();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public int lastid(){
        int max = 0;
     
     ResultSet result = null;
         try {
             Connection con = Connect.getConnection();
             result = con.createStatement().executeQuery("SELECT MAX(idcmd) as maximum FROM commande");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
         } catch (Exception e) {
         }
     
        
        return max;
    }
}
