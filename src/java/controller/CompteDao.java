/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Compte;

/**
 *
 * @author Administrateur
 */
public class CompteDao  extends Dao <Compte> {

    @Override
    public Compte rechercher(long id) {
        ResultSet result = null;
        Compte cpt = new Compte();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM compte WHERE idcpt ="+id);
            if(result.next() ){
                cpt.setIdc(result.getInt("idcp"));
                cpt.setLogin(result.getString("login"));
                cpt.setMdp(result.getString("mdp"));
                cpt.setRole(result.getString("role"));
                cpt.setQuestion(result.getString("question"));
                cpt.setResponse(result.getString("response"));
                cpt.setDateCreation(result.getDate("date_creation"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CompteDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return cpt;
    }

    @Override
    public void inserer(Compte cpt) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `compte`( `login`, `mdp`, `role`, `question`, `response`, `date_creation`) VALUES (?,?,?,?,?,?)");
            prepare.setString(1, cpt.getLogin());
            prepare.setString(2, cpt.getMdp());
            prepare.setString(3, cpt.getRole());
            prepare.setString(4, cpt.getQuestion());
            prepare.setString(5, cpt.getResponse());
            prepare.setString(6, amj.format(cpt.getDateCreation()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CompteDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void modifier(Compte cpt, long id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
         try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `compte` SET `login`=?,`mdp`=?,`role`=?,`question`=?,`response`=?,`date_creation`=? WHERE idcpt"+id);
            prepare.setString(1, cpt.getLogin());
            prepare.setString(2, cpt.getMdp());
            prepare.setString(3, cpt.getRole());
            prepare.setString(4, cpt.getQuestion());
            prepare.setString(5, cpt.getResponse());
            prepare.setString(6, amj.format(cpt.getDateCreation()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CompteDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM compte WHERE idcpt ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CompteDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Compte> liste() {
         List<Compte> Compte = new ArrayList();
        Compte obj = new Compte();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM compte ");
            while(result.next()){
            obj.setIdc(result.getInt("idcpt"));
            obj.setLogin(result.getString("login"));
            obj.setMdp(result.getString("mdp"));
            obj.setRole(result.getString("role"));
            obj.setQuestion(result.getString("question"));
            obj.setResponse(result.getString("response"));
            obj.setDateCreation(result.getDate("date_creation"));
            Compte.add(obj);
            obj = new Compte();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CompteDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Compte;
    }
    
    public int lastid(){
        int max = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT MAX(idcpt) as maximum FROM compte");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CompteDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return max;
    }
    
}
