/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Client;

/**
 *
 * @author Administrateur
 */
public class ClientDao extends Dao <Client> {

    @Override
    public Client rechercher(long id) {
        ResultSet result = null;
        Client c = new Client();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM client WHERE idc ="+id);
            if(result.next() ){
                c.setIdc(result.getInt("idc"));
                c.setIdcpt(result.getInt("idcpt"));
                c.setNom(result.getString("nom"));
                c.setPrenom(result.getString("prenom"));
                c.setEmail(result.getString("email"));
                c.setTel(result.getString("tel"));
                c.setRegDate(result.getDate("regDate"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClientDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return c;
    }

    @Override
    public void inserer(Client c) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `client`( `idcpt`, `nom`, `prenom`, `email`, `tel`, `regDate`) VALUES (?,?,?,?,?,?)");
            prepare.setString(1, String.valueOf(c.getIdcpt()));
            prepare.setString(2, c.getNom());
            prepare.setString(3, c.getPrenom());
            prepare.setString(4, c.getEmail());
            prepare.setString(5, c.getTel());
            prepare.setString(6, amj.format(c.getRegDate()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Client c, long id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
         try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `client` SET `idcpt`=?,`nom`=?,`prenom`=?,`email`=?,`tel`=?,`regDate`=? WHERE idc"+id);
            prepare.setString(1, String.valueOf(c.getIdcpt()));
            prepare.setString(2, c.getNom());
            prepare.setString(3, c.getPrenom());
            prepare.setString(4, c.getEmail());
            prepare.setString(5, c.getTel());
            prepare.setString(6, amj.format(c.getRegDate()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM client WHERE idc ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Client> liste() {
        List<Client> Client = new ArrayList();
        Client obj = new Client();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM client ");
            while(result.next()){
            obj.setIdc(result.getInt("idc"));
            obj.setIdcpt(result.getInt("idcmpt"));
            obj.setNom(result.getString("nom"));
            obj.setPrenom(result.getString("prenom"));
            obj.setEmail(result.getString("email"));
            obj.setTel(result.getString("tel"));
            obj.setRegDate(result.getDate("regDate"));
            Client.add(obj);
            obj = new Client();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ClientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Client;
    }
    
}
