/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Categorie;

/**
 *
 * @author Administrateur
 */
public class CategoryMgr {

    public CategoryMgr() {
    }
    
    public void add(Categorie c){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `categorie`( `libelle`, `description`) VALUES (?,?)");
            prepare.setString(1, c.getLibelle());
            prepare.setString(2, c.getDescription());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Categorie c, String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("ie` SET `idcat`=[value-1],`libelle`=[value-2],`description`=[value-3] WHERE idcat"+id);
            prepare.setString(1, c.getLibelle());
            prepare.setString(2, c.getDescription());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM categorie WHERE idcat ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Categorie> list(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Categorie obj = new Categorie();
            
             rs = con.createStatement().executeQuery("SELECT * FROM `categorie`");
            while(rs.next()){
            obj.setIdcat(rs.getInt("idcat"));
            obj.setLibelle(rs.getString("libelle"));
            obj.setDescription(rs.getString("description"));
                
                liste.add(obj);
                obj = new Categorie();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public ArrayList<Categorie> list2(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Categorie obj = new Categorie();
            
             rs = con.createStatement().executeQuery("SELECT * FROM categorie order by idp desc limit 0,9");
            while(rs.next()){
            obj.setIdcat(rs.getInt("idcat"));
            obj.setLibelle(rs.getString("libelle"));
            obj.setDescription(rs.getString("description"));
                
                liste.add(obj);
                obj = new Categorie();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
}
