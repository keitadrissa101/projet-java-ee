/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Categorie;

/**
 *
 * @author Administrateur
 */
public class CategoryDao extends Dao <Categorie> {

    @Override
    public Categorie rechercher(long id) {
       ResultSet result = null;
        Categorie c = new Categorie();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM categorie WHERE idcat ="+id);
            if(result.next() ){
                c.setIdcat(result.getInt("idcat"));
                c.setLibelle(result.getString("libelle"));
                c.setDescription(result.getString("description"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return c;
    }

    @Override
    public void inserer(Categorie c) {
       
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `categorie`( `libelle`, `description`) VALUES (?,?)");
            prepare.setString(1, c.getLibelle());
            prepare.setString(2, c.getDescription());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Categorie c, long id) {
       try {
            PreparedStatement prepare = this.connect.prepareStatement("ie` SET `idcat`=[value-1],`libelle`=[value-2],`description`=[value-3] WHERE idcat"+id);
            prepare.setString(1, c.getLibelle());
            prepare.setString(2, c.getDescription());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
       try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM categorie WHERE idcat ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Categorie> liste() {
       List<Categorie> Categorie = new ArrayList();
        Categorie obj = new Categorie();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM categorie ");
            while(result.next()){
            obj.setIdcat(result.getInt("idcat"));
            obj.setLibelle(result.getString("libelle"));
            obj.setDescription(result.getString("description"));
            Categorie.add(obj);
            obj = new Categorie();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Categorie;
    }
    
}
