/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Commentaire;

/**
 *
 * @author Administrateur
 */
public class CommentaireDao extends Dao <Commentaire> {

    @Override
    public Commentaire rechercher(long id) {
        ResultSet result = null;
        Commentaire c = new Commentaire();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM commentaire WHERE idcomm ="+id);
            if(result.next() ){
                c.setIdcomm(result.getInt("idcomm"));
                c.setIdp(result.getInt("idp"));
                c.setIdcmd(result.getInt("idcmd"));
                c.setCommentaire(result.getString("commentaire"));
                c.setDatePub(result.getDate("date_pub"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return c;
    }

    @Override
    public void inserer(Commentaire c) {
       SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `commentaire`( `idp`, `idcmd`, `commentaire`, `date_pub`) VALUES (?,?,?,?)");
            prepare.setString(1, String.valueOf(c.getIdp()));
            prepare.setString(2, String.valueOf(c.getIdcmd()));
            prepare.setString(3, c.getCommentaire());
            prepare.setString(4, amj.format(c.getDatePub()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Commentaire c, long id) {
       SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
         try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `commentaire` SET `idp`=?,`idcmd`=?,`commentaire`=?,`date_pub`=? WHERE idcmd"+id);
            prepare.setString(1, String.valueOf(c.getIdp()));
            prepare.setString(2, String.valueOf(c.getIdcmd()));
            prepare.setString(3, c.getCommentaire());
            prepare.setString(4, amj.format(c.getDatePub()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM commentaire WHERE idcomm ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Commentaire> liste() {
       List<Commentaire> Commentaire = new ArrayList();
        Commentaire obj = new Commentaire();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM commentaire ");
            while(result.next()){
            obj.setIdp(result.getInt("idp"));
            obj.setIdcmd(result.getInt("idcmd"));
            obj.setCommentaire(result.getString("commentaire"));
            obj.setDatePub(result.getDate("date_pub"));
            Commentaire.add(obj);
            obj = new Commentaire();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Commentaire;
    }
    
}
