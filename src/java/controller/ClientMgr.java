/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Client;

/**
 *
 * @author Administrateur
 */
public class ClientMgr {

    public ClientMgr() {
    }
    
    public Client rechercher(int id) {
        ResultSet result = null;
        Client c = new Client();
        
        try {
            Connection con = Connect.getConnection();
            result = con.createStatement().executeQuery("SELECT * FROM client WHERE idc ="+id);
            if(result.next() ){
                c.setIdc(result.getInt("idc"));
                c.setIdcpt(result.getInt("idcpt"));
                c.setNom(result.getString("nom"));
                c.setPrenom(result.getString("prenom"));
                c.setEmail(result.getString("email"));
                c.setTel(result.getString("tel"));
                c.setRegDate(result.getDate("regDate"));
                
                
            }
        } catch (Exception e) {
        }
        
        
        return c;
    }
    
    
        public void add(Client c){
            SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `client`( `idcpt`, `nom`, `prenom`, `email`, `tel`, `regDate`) VALUES (?,?,?,?,?,?)");
            prepare.setInt(1, c.getIdcpt());
            prepare.setString(2, c.getNom());
            prepare.setString(3, c.getPrenom());
            prepare.setString(4, c.getEmail());
            prepare.setString(5, c.getTel());
            prepare.setString(6, amj.format(c.getRegDate()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Client c, String id){
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `client` SET `idcpt`=?,`nom`=?,`prenom`=?,`email`=?,`tel`=?,`regDate`=? WHERE idc"+id);
            prepare.setInt(1, c.getIdcpt());
            prepare.setString(2, c.getNom());
            prepare.setString(3, c.getPrenom());
            prepare.setString(4, c.getEmail());
            prepare.setString(5, c.getTel());
            prepare.setString(6, amj.format(c.getRegDate()));
            prepare.executeUpdate();

            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM client WHERE idc ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Client> list(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Client obj = new Client();
            
             rs = con.createStatement().executeQuery("SELECT * FROM client ");
            while(rs.next()){
            obj.setIdc(rs.getInt("idc"));
            obj.setIdcpt(rs.getInt("idcmpt"));
            obj.setNom(rs.getString("nom"));
            obj.setPrenom(rs.getString("prenom"));
            obj.setEmail(rs.getString("email"));
            obj.setTel(rs.getString("tel"));
            obj.setRegDate(rs.getDate("regDate"));
                
                liste.add(obj);
                obj = new Client();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public int lastid(){
        int max = 0;
     Connection con = null;
     ResultSet result = null;
         try {
             result = con.createStatement().executeQuery("SELECT MAX(ida) as maximum FROM client");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
         } catch (Exception e) {
         }
     
        
        return max;
    }
}
