/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Expedition;

/**
 *
 * @author Administrateur
 */
public class ExpeditionMgr {

    public ExpeditionMgr() {
    }
    
    
    public Expedition rechercher(long id) {
        ResultSet result = null;
        Expedition ex = new Expedition();
        try {
            Connection con = Connect.getConnection();
            result = con.createStatement().executeQuery("SELECT * FROM expedition WHERE idcmd ="+id);
            if(result.next() ){
                ex.setIdex(result.getInt("idcmd"));
                ex.setMoyenExpedition(result.getString("moyen_expedition"));
                ex.setNumeroExpedition(result.getString("numero_expedition"));
                ex.setEtat(result.getString("etat"));
                
                
            }
        } catch (Exception e) {
            
        } 
        return ex;
    }
    
        public void add(Expedition ex){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `expedition`(`idcmd` `moyen_expedition`, `numero_expedition`, `etat`) VALUES (?,?,?,?)");
            prepare.setInt(1, ex.getIdcmd());
            prepare.setString(2, ex.getMoyenExpedition());
            prepare.setString(3, ex.getNumeroExpedition());
            prepare.setString(4, ex.getEtat());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Expedition ex, String id){
        try {
            Connection con = Connect.getConnection();
             PreparedStatement prepare = con.prepareStatement("UPDATE `expedition` SET `idcmd`=? `moyen_expedition`=?,`numero_expedition`=?,`etat`=? WHERE idex"+id);
            prepare.setInt(1, ex.getIdcmd());
            prepare.setString(2, ex.getMoyenExpedition());
            prepare.setString(3, ex.getNumeroExpedition());
            prepare.setString(4, ex.getEtat());
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
             PreparedStatement prepare = con.prepareStatement("DELETE FROM expedition WHERE idex ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Expedition> listCourse(){
        Connection con = null;
        ResultSet result = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Expedition obj = new Expedition();
            
            result = con.createStatement().executeQuery("SELECT * FROM expedition ");
            while(result.next()){
            obj.setIdex(result.getInt("idcmd"));
            obj.setMoyenExpedition(result.getString("moyen_expedition"));
            obj.setNumeroExpedition(result.getString("numero_expedition"));
            obj.setEtat(result.getString("etat"));
                
                liste.add(obj);
                obj = new Expedition();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
}
