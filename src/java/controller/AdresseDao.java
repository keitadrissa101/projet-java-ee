/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Adresse;
/**
 *
 * @author Administrateur
 */
public class AdresseDao extends Dao <Adresse> {

    @Override
    public Adresse rechercher(long id) {
        ResultSet result = null;
        Adresse a = new Adresse();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM adresse WHERE ida ="+id);
            if(result.next() ){
                a.setIda(result.getInt("ida"));
                a.setAd(result.getString("adresse"));
                a.setVille(result.getString("ville"));
                a.setCodepostale(result.getInt("codepostale"));
                a.setPays(result.getString("pays"));
                a.setIdc(result.getInt("idc"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdresseDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return a;
    }

    @Override
    public void inserer(Adresse a) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `adresse`( `adresse`, `ville`, `codepostale`, `pays`, `idc`) VALUES (?,?,?,?,?)");
            prepare.setString(1, String.valueOf(a.getIda()));
            prepare.setString(2, a.getAd());
            prepare.setString(3, a.getVille());
            prepare.setString(4, a.getPays());
            prepare.setString(5, String.valueOf(a.getCodepostale()));
            prepare.setString(6, String.valueOf(a.getIdc()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdresseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Adresse a, long id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
         try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `adresse` SET `adresse`=?,`ville`=?,`codepostale`=?,`pays`=?,`idc`=? WHERE ida"+id);
            prepare.setString(1, String.valueOf(a.getIda()));
            prepare.setString(2, a.getAd());
            prepare.setString(3, a.getVille());
            prepare.setString(4, a.getPays());
            prepare.setString(5, String.valueOf(a.getCodepostale()));
            prepare.setString(6, String.valueOf(a.getIdc()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdresseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM adresse WHERE ida ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdresseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Adresse> liste() {
        List<Adresse> Adresse = new ArrayList();
        Adresse obj = new Adresse();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM adresse ");
            while(result.next()){
            obj.setIda(result.getInt("ida"));
            obj.setAd(result.getString("adresse"));
            obj.setVille(result.getString("ville"));
            obj.setPays(result.getString("pays"));
            obj.setCodepostale(result.getInt("codepostale"));
            obj.setIdc(result.getInt("idc"));
            Adresse.add(obj);
            obj = new Adresse();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Adresse;
    }
    
}
