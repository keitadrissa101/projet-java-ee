/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Produit;

/**
 *
 * @author Administrateur
 */
public class ProduitMgr {

    public ProduitMgr() {
    }
        
        public Produit research(int id){
            Connection con = null;
            ResultSet result = null;
            Produit  p = new Produit();
        
            try {
                 
            con = Connect.getConnection();
            result = con.createStatement().executeQuery("SELECT * FROM `produit` WHERE `idp`="+id);
            if (result.next()) {
            p.setIdp(result.getInt("idp"));
            p.setIdcat(result.getInt("idcat"));
            p.setLibelle(result.getString("libelle"));
            p.setCountry(result.getString("country"));
            p.setPrix(result.getInt("prix"));
            p.setDescription(result.getString("description"));
            p.setQtestck(result.getInt("qtestck"));
            p.setDatePub(result.getDate("date_pub"));
                }
            } catch (Exception e) {
            }
        
            return p;
        }
    
        public void add(Produit p){
            SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `produit`(`idcat`, `libelle`, `country`, `prix`, `description`, `qtestck`, `date_pub`) VALUES (?,?,?,?,?,?,?)");
            prepare.setInt(1, p.getIdcat());
            prepare.setString(2, p.getLibelle());
            prepare.setString(3, p.getCountry());
            prepare.setInt(4, p.getPrix());
            prepare.setString(5, p.getDescription());
            prepare.setInt(6, p.getQtestck());
            prepare.setString(7, amj.format(p.getDatePub()));
            prepare.executeUpdate();

            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Produit p, String id){
         SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `produit` SET `idcat`=?,`libelle`=?,`country`=?,`prix`=?,`description`=?,`qtestck`=?,`date_pub`=? WHERE idp"+id);
            prepare.setInt(1, p.getIdcat());
            prepare.setString(2, p.getLibelle());
            prepare.setString(3, p.getCountry());
            prepare.setInt(4, p.getPrix());
            prepare.setString(5, p.getDescription());;
            prepare.setInt(6, p.getQtestck());
            prepare.setString(7, amj.format(p.getDatePub()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void del(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM produit WHERE idp ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Produit> list(){
        Connection con = null;
        ResultSet result = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Produit obj = new Produit();
            
            result = con.createStatement().executeQuery("SELECT * FROM `produit`");
            while(result.next()){
            obj.setIdp(result.getInt("idp"));
            obj.setIdcat(result.getInt("idcat"));
            obj.setLibelle(result.getString("libelle"));
            obj.setCountry(result.getString("country"));
            obj.setPrix(result.getInt("prix"));
            obj.setDescription(result.getString("description"));
            obj.setQtestck(result.getInt("qtestck"));
            obj.setDatePub(result.getDate("date_pub"));
                
                liste.add(obj);
                obj = new Produit();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public ArrayList<Produit> list2(){
        Connection con = null;
        ResultSet result = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Produit obj = new Produit();
            
            result = con.createStatement().executeQuery("SELECT * FROM `produit` order by idp desc limit 0,4");
            while(result.next()){
            obj.setIdp(result.getInt("idp"));
            obj.setIdcat(result.getInt("idcat"));
            obj.setLibelle(result.getString("libelle"));
            obj.setCountry(result.getString("country"));
            obj.setPrix(result.getInt("prix"));
            obj.setDescription(result.getString("description"));
            obj.setQtestck(result.getInt("qtestck"));
            obj.setDatePub(result.getDate("date_pub"));
                
                liste.add(obj);
                obj = new Produit();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    public ArrayList<Produit> listr(String sql){
        Connection con = null;
        ResultSet result = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Produit obj = new Produit();
            
            
            result = con.createStatement().executeQuery(sql);
            while(result.next()){
            obj.setIdp(result.getInt("idp"));
            obj.setIdcat(result.getInt("idcat"));
            obj.setLibelle(result.getString("libelle"));
            obj.setCountry(result.getString("country"));
            obj.setPrix(result.getInt("prix"));
            obj.setDescription(result.getString("description"));
            obj.setQtestck(result.getInt("qtestck"));
            obj.setDatePub(result.getDate("date_pub"));
                
                liste.add(obj);
                obj = new Produit();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    
    
     public int lastid(){
        int max = 0;
     Connection con = null;
     ResultSet result = null;
         try {
             con = Connect.getConnection();
             result = con.createStatement().executeQuery("SELECT MAX(idp) as maximum FROM produit");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
         } catch (Exception e) {
         }
     
        
        return max;
    }
     
     public int GetPrix(int idp){
        int prix = 0;
     Connection con = null;
     ResultSet result = null;
         try {
             result = con.createStatement().executeQuery("SELECT prix FROM produit where idp="+idp);
            if(result.next() ){
              prix = result.getInt("prix");
                
            }
         } catch (Exception e) {
         }
            
        return prix;
    }
}
