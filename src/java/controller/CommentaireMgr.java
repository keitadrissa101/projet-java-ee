/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Commentaire;

/**
 *
 * @author Administrateur
 */
public class CommentaireMgr {

    public CommentaireMgr() {
    }
    
        public void addCourse(Commentaire c){
            SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `commentaire`( `idp`, `idcmd`, `commentaire`, `date_pub`) VALUES (?,?,?,?)");
            prepare.setInt(1, c.getIdp());
            prepare.setInt(2, c.getIdcmd());
            prepare.setString(3, c.getCommentaire());
            prepare.setString(4, amj.format(c.getDatePub()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void editCourse (Commentaire c, String id){
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `commentaire` SET `idp`=?,`idcmd`=?,`commentaire`=?,`date_pub`=? WHERE idcmd"+id);
            prepare.setString(1, String.valueOf(c.getIdp()));
            prepare.setString(2, String.valueOf(c.getIdcmd()));
            prepare.setString(3, c.getCommentaire());
            prepare.setString(4, amj.format(c.getDatePub()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void delCourse(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM commentaire WHERE idcomm ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Commentaire> listCourse(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Commentaire obj = new Commentaire();
            
             rs = con.createStatement().executeQuery("SELECT * FROM commentaire ");
            while(rs.next()){
            obj.setIdp(rs.getInt("idp"));
            obj.setIdcmd(rs.getInt("idcmd"));
            obj.setCommentaire(rs.getString("commentaire"));
            obj.setDatePub(rs.getDate("date_pub"));
                
                liste.add(obj);
                obj = new Commentaire();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
}
