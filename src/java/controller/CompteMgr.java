/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import BD.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.Compte;

/**
 *
 * @author Administrateur
 */
public class CompteMgr {

    public CompteMgr() {
    }
    
    
     public Compte rechercher(long id) {
        ResultSet result = null;
        Compte cpt = new Compte();
        
         try {
             Connection con = Connect.getConnection();
             result = con.createStatement().executeQuery("SELECT * FROM `compte` WHERE idcpt ="+id);
            if(result.next() ){
                cpt.setIdc(result.getInt("idcpt"));
                cpt.setLogin(result.getString("login"));
                cpt.setMdp(result.getString("mdp"));
                cpt.setRole(result.getString("role"));
                cpt.setQuestion(result.getString("question"));
                cpt.setResponse(result.getString("response"));
                cpt.setDateCreation(result.getDate("date_creation"));
            }
         } catch (Exception e) {
         }
        
        return cpt;
    }
    
        public void add(Compte cpt){
            SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("INSERT INTO `compte`( `login`, `mdp`, `role`, `question`, `response`, `date_creation`) VALUES (?,?,?,?,?,?)");
            prepare.setString(1, cpt.getLogin());
            prepare.setString(2, cpt.getMdp());
            prepare.setString(3, cpt.getRole());
            prepare.setString(4, cpt.getQuestion());
            prepare.setString(5, cpt.getResponse());
            prepare.setString(6, amj.format(cpt.getDateCreation()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void edit (Compte cpt, String id){
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("UPDATE `compte` SET `login`=?,`mdp`=?,`role`=?,`question`=?,`response`=?,`date_creation`=? WHERE idcpt"+id);
            prepare.setString(1, cpt.getLogin());
            prepare.setString(2, cpt.getMdp());
            prepare.setString(3, cpt.getRole());
            prepare.setString(4, cpt.getQuestion());
            prepare.setString(5, cpt.getResponse());
            prepare.setString(6, amj.format(cpt.getDateCreation()));
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public void delCourse(String id){
        try {
            Connection con = Connect.getConnection();
            PreparedStatement prepare = con.prepareStatement("DELETE FROM compte WHERE idcpt ="+id);
            prepare.executeUpdate();
            
            prepare.close();
            prepare = null;
            con.close();
            con = null;
        } catch (Exception e) {
        }
    }
    
    public ArrayList<Compte> list(){
        Connection con = null;
        ResultSet rs = null;
        ArrayList liste = new ArrayList();
        try {
            con = Connect.getConnection();
            
            
            Compte obj = new Compte();
            
             rs = con.createStatement().executeQuery("SELECT * FROM compte ");
            while(rs.next()){
            obj.setIdc(rs.getInt("idcpt"));
            obj.setLogin(rs.getString("login"));
            obj.setMdp(rs.getString("mdp"));
            obj.setRole(rs.getString("role"));
            obj.setQuestion(rs.getString("question"));
            obj.setResponse(rs.getString("response"));
            obj.setDateCreation(rs.getDate("date_creation"));
            
                liste.add(obj);
                obj = new Compte();
            }
            
        } catch (Exception e) {
        }
        
        return liste;
    }
    
    
    public int lastid(){
        int max = 0;
     
     ResultSet result = null;
         try {
             Connection con = Connect.getConnection();
             result = con.createStatement().executeQuery("SELECT MAX(idcpt) as maximum FROM compte     é");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
         } catch (Exception e) {
         }
     
        
        return max;
    }
}
