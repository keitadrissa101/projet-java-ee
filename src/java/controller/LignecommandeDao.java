/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Lignecommande;

/**
 *
 * @author Administrateur
 */
public class LignecommandeDao extends Dao <Lignecommande> {

    @Override
    public Lignecommande rechercher(long id) {
        ResultSet result = null;
        Lignecommande lc = new Lignecommande();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM lignecommande WHERE idcmd ="+id);
            if(result.next() ){
                lc.setIdlc(result.getInt("idlc"));
                lc.setIdcm(result.getInt("idcmd"));
                lc.setIdp(result.getInt("idp"));
                lc.setQte(result.getInt("qte"));
                lc.setPrixAchat(result.getInt("prix_achat"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(LignecommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return lc;
    }

    @Override
    public void inserer(Lignecommande lc) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO `lignecommande`(`idcmd`, `idp`, `qte`, `prix_achat`) VALUES (?,?,?,?)");
            prepare.setString(1, String.valueOf(lc.getIdlc()));
            prepare.setString(2, String.valueOf(lc.getIdcm()));
            prepare.setString(3, String.valueOf(lc.getIdp()));
            prepare.setString(4, String.valueOf(lc.getQte()));
            prepare.setString(5, String.valueOf(lc.getPrixAchat()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(LignecommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Lignecommande lc, long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE `lignecommande` SET `idcmd`=?,`idp`=?,`qte`=?,`prix_achat`=? WHERE idLC"+id);
            prepare.setString(1, String.valueOf(lc.getIdlc()));
            prepare.setString(2, String.valueOf(lc.getIdcm()));
            prepare.setString(3, String.valueOf(lc.getIdp()));
            prepare.setString(4, String.valueOf(lc.getQte()));
            prepare.setString(5, String.valueOf(lc.getPrixAchat()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(LignecommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM lignecommande WHERE idlc ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(LignecommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Lignecommande> liste() {
        List<Lignecommande> Lignecommande = new ArrayList();
        Lignecommande obj = new Lignecommande();
        ResultSet result; 
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM lignecommande ");
            while(result.next()){
            obj.setIdlc(result.getInt("idlc"));
            obj.setIdcm(result.getInt("idcmd"));
            obj.setIdp(result.getInt("idp"));
            obj.setQte(result.getInt("qte"));
            obj.setPrixAchat(result.getInt("prix_achat"));
            Lignecommande.add(obj);
            obj = new Lignecommande();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(LignecommandeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Lignecommande;
    }
    
}
