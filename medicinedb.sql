-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 05 jan. 2021 à 11:33
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `medicinedb`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse` (
  `ida` int(11) NOT NULL,
  `ad` varchar(100) DEFAULT NULL,
  `ville` varchar(100) DEFAULT NULL,
  `codepostale` int(11) DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  `idc` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`ida`, `ad`, `ville`, `codepostale`, `pays`, `idc`) VALUES
(45, 'Door JavaEE', 'India', 20, 'China', 4);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `idcat` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`idcat`, `libelle`, `description`) VALUES
(1, 'Respiratory', NULL),
(2, 'Analgesics', NULL),
(3, 'Gastrointestinal', NULL),
(4, 'Circulatory', NULL),
(5, 'Eye/ear', NULL),
(6, 'Oral hygienne', NULL),
(7, 'Amtiparasitic', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idc` int(11) NOT NULL,
  `idcpt` int(11) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `regDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idc`, `idcpt`, `nom`, `prenom`, `email`, `tel`, `regDate`) VALUES
(1, 1, 'Administrator', 'Administrator', 'admin@gmail.com', NULL, '2020-12-15'),
(2, 2, 'Mohamed', 'KONe', 'moh@gmail.com', NULL, '2020-12-15'),
(3, 3, 'Mozart', 'Sage', 'mozart@gmail.com', NULL, '2021-01-04'),
(4, 4, 'Momo', 'Mozart', 'momo@gmail.com', NULL, '2021-01-04');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `idcmd` int(11) NOT NULL,
  `idc` int(11) DEFAULT NULL,
  `ida` int(11) DEFAULT NULL,
  `datecmd` date DEFAULT NULL,
  `methode_pay` varchar(30) DEFAULT NULL,
  `etat` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`idcmd`, `idc`, `ida`, `datecmd`, `methode_pay`, `etat`) VALUES
(35, 4, 45, '2021-01-04', 'VISA', 'Get');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `idcomm` int(11) NOT NULL,
  `idp` int(11) DEFAULT NULL,
  `idcmd` int(11) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `date_pub` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `idcpt` int(11) NOT NULL,
  `login` varchar(50) DEFAULT NULL,
  `mdp` varchar(50) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `question` varchar(50) DEFAULT NULL,
  `response` varchar(50) DEFAULT NULL,
  `date_creation` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`idcpt`, `login`, `mdp`, `role`, `question`, `response`, `date_creation`) VALUES
(1, 'admin@gmail.com', '121212', 'admin', NULL, NULL, '2020-12-15'),
(2, 'moh@gmail.com', '123123', 'client', NULL, NULL, '2020-12-15'),
(3, 'mozart@gmail.com', '123456', 'client', NULL, NULL, '2021-01-04'),
(4, 'momo@gmail.com', '111111', 'client', NULL, NULL, '2021-01-04');

-- --------------------------------------------------------

--
-- Structure de la table `expedition`
--

CREATE TABLE `expedition` (
  `idex` int(11) NOT NULL,
  `idcmd` int(11) NOT NULL,
  `moyen_expedition` varchar(100) DEFAULT NULL,
  `numero_expedition` varchar(100) DEFAULT NULL,
  `etat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `expedition`
--

INSERT INTO `expedition` (`idex`, `idcmd`, `moyen_expedition`, `numero_expedition`, `etat`) VALUES
(4, 31, 'Car', 'cm201', 'expedite'),
(5, 34, 'Car', 'll2', 'expedite'),
(6, 35, 'Car', 'cmd42exp', 'expedite');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `idimg` int(11) NOT NULL,
  `idp` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`idimg`, `idp`, `url`, `filename`) VALUES
(18, 32, 'images/ressources\\product1.jpg', 'product1.jpg'),
(19, 33, 'images/ressources\\product2.jpg', 'product2.jpg'),
(21, 35, 'images/ressources\\product3.jpg', 'product3.jpg'),
(22, 36, 'images/ressources\\one.jpg', 'one.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `itemwishlist`
--

CREATE TABLE `itemwishlist` (
  `idw` int(11) NOT NULL,
  `idp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `lignecommande`
--

CREATE TABLE `lignecommande` (
  `idlc` int(11) NOT NULL,
  `idcmd` int(11) DEFAULT NULL,
  `idp` int(11) DEFAULT NULL,
  `qte` int(11) DEFAULT NULL,
  `prix_achat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `lignecommande`
--

INSERT INTO `lignecommande` (`idlc`, `idcmd`, `idp`, `qte`, `prix_achat`) VALUES
(5, 35, 35, 4, 120),
(6, 35, 32, 1, 20);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `idp` int(11) NOT NULL,
  `idcat` int(11) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `qtestck` int(11) DEFAULT NULL,
  `date_pub` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`idp`, `idcat`, `libelle`, `country`, `prix`, `description`, `qtestck`, `date_pub`) VALUES
(32, 2, 'Adderal', 'France', 20, 'just for child', 2000, '2021-01-04'),
(33, 1, 'Ativan', 'China', 35, 'For old mane to not place abouve 30 degree', 4000, '2021-01-04'),
(35, 4, 'Lyrica', 'India', 120, 'Use for all kind of Circulatory deases', 3000, '2021-01-04'),
(36, 1, 'Covid-19', 'China', 100, '', 4000, '2021-01-04');

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

CREATE TABLE `promotion` (
  `idprom` int(11) NOT NULL,
  `idp` int(11) DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date NOT NULL,
  `taux` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `seq_clt`
--

CREATE TABLE `seq_clt` (
  `next_not_cached_value` bigint(21) NOT NULL,
  `minimum_value` bigint(21) NOT NULL,
  `maximum_value` bigint(21) NOT NULL,
  `start_value` bigint(21) NOT NULL COMMENT 'start value when sequences is created or value if RESTART is used',
  `increment` bigint(21) NOT NULL COMMENT 'increment value',
  `cache_size` bigint(21) UNSIGNED NOT NULL,
  `cycle_option` tinyint(1) UNSIGNED NOT NULL COMMENT '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
  `cycle_count` bigint(21) NOT NULL COMMENT 'How many cycles have been done'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `wishlist`
--

CREATE TABLE `wishlist` (
  `idw` int(11) NOT NULL,
  `idc` int(11) DEFAULT NULL,
  `libelle` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`ida`),
  ADD KEY `idc` (`idc`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`idcat`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idc`),
  ADD KEY `idcpt` (`idcpt`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`idcmd`),
  ADD KEY `idc` (`idc`),
  ADD KEY `ida` (`ida`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`idcomm`),
  ADD KEY `idp` (`idp`),
  ADD KEY `idcmd` (`idcmd`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`idcpt`);

--
-- Index pour la table `expedition`
--
ALTER TABLE `expedition`
  ADD PRIMARY KEY (`idex`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`idimg`),
  ADD KEY `idp` (`idp`);

--
-- Index pour la table `itemwishlist`
--
ALTER TABLE `itemwishlist`
  ADD PRIMARY KEY (`idw`,`idp`),
  ADD KEY `idp` (`idp`);

--
-- Index pour la table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD PRIMARY KEY (`idlc`),
  ADD KEY `idcmd` (`idcmd`),
  ADD KEY `idp` (`idp`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`idp`),
  ADD KEY `idcat` (`idcat`);

--
-- Index pour la table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`idprom`),
  ADD KEY `idp` (`idp`);

--
-- Index pour la table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`idw`),
  ADD KEY `idc` (`idc`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `ida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `idcat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `idcmd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `idcomm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `idcpt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `expedition`
--
ALTER TABLE `expedition`
  MODIFY `idex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `idimg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `lignecommande`
--
ALTER TABLE `lignecommande`
  MODIFY `idlc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `idp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT pour la table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `idprom` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `idw` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD CONSTRAINT `adresse_ibfk_1` FOREIGN KEY (`idc`) REFERENCES `client` (`idc`);

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`idcpt`) REFERENCES `compte` (`idcpt`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_2` FOREIGN KEY (`idc`) REFERENCES `client` (`idc`),
  ADD CONSTRAINT `commande_ibfk_3` FOREIGN KEY (`ida`) REFERENCES `adresse` (`ida`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`idp`) REFERENCES `produit` (`idp`),
  ADD CONSTRAINT `commentaire_ibfk_2` FOREIGN KEY (`idcmd`) REFERENCES `commande` (`idcmd`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`idp`) REFERENCES `produit` (`idp`);

--
-- Contraintes pour la table `itemwishlist`
--
ALTER TABLE `itemwishlist`
  ADD CONSTRAINT `itemwishlist_ibfk_1` FOREIGN KEY (`idw`) REFERENCES `wishlist` (`idw`),
  ADD CONSTRAINT `itemwishlist_ibfk_2` FOREIGN KEY (`idp`) REFERENCES `produit` (`idp`);

--
-- Contraintes pour la table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD CONSTRAINT `lignecommande_ibfk_1` FOREIGN KEY (`idcmd`) REFERENCES `commande` (`idcmd`),
  ADD CONSTRAINT `lignecommande_ibfk_2` FOREIGN KEY (`idp`) REFERENCES `produit` (`idp`);

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`idcat`) REFERENCES `categorie` (`idcat`);

--
-- Contraintes pour la table `promotion`
--
ALTER TABLE `promotion`
  ADD CONSTRAINT `promotion_ibfk_1` FOREIGN KEY (`idp`) REFERENCES `produit` (`idp`);

--
-- Contraintes pour la table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`idc`) REFERENCES `client` (`idc`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
