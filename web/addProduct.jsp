<%-- 
    Document   : addProduct
    Created on : 14 déc. 2020, 18:00:55
    Author     : Administrateur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        <% 
           
            if (request.getAttribute("mgs")!=null) {
                
        %>
        <h1 style="color: red"><%=request.getAttribute("mgs")%></h1>
         <%
                }
        %>

        <section id=""><!--form-->
            <div class="container">
                <div class="row">				
                    <div class="col-sm-10">
                        <div class="signup-form"><!--sign up form-->
                            <h2>New Product !</h2>
                            <form action="AddProductServlet" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Medicine Name" />
                                </div>
                                <div class="form-group">
                                    <select name="category" class="form-group">
	                                <option value="1">Respiratory</option>
                                        <option value="2">Analgesics</option>
                                        <option value="3">Gastrointestinal</option>
                                        <option value="4">Circulatory</option>
                                        <option value="5">Eye/ear</option>
                                        <option value="6">Oral hygienne</option>
                                        <option value="7">Amtiparasitic</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="country" class="form-group">
	                                <option value="China">China</option>
                                        <option value="India">India</option>
                                        <option value="France">France</option>
                                        <option value="Nigeria">Nigeria</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="description" placeholder="Description" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="price" placeholder="price"/>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="quantitty" placeholder="Quantity"/>
                                </div>
                                <div class="form-group">
                                
                                <div class="form-group">
                                    <input type="file" name="imag" placeholder="Image"/>
                                </div>
                                <button type="submit" class="btn btn-default">Save</button>
                            </form><br>
                        </div><!--/sign up form-->
                    </div>
                </div>
            </div>
	</section><!--/form-->
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
