<%-- 
    Document   : detailCommande
    Created on : 26 nov. 2020, 13:30:03
    Author     : Administrateur
--%>




<%@page import="model.Image"%>
<%@page import="controller.ImageMgr"%>
<%@page import="controller.LignecommandeMgr"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Produit"%>
<%@page import="controller.ProduitMgr"%>
<%@page import="model.Lignecommande"%>
<%@page import="model.Expedition"%>
<%@page import="controller.ExpeditionMgr"%>
<%@page import="model.Commande"%>
<%@page import="controller.CommandeMgr"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
          <jsp:include page="/views/header.jsp"></jsp:include>
          
          <%
                        
                    int idcmd = Integer.parseInt(request.getParameter("idcmd"));
                    CommandeMgr cmgr = new CommandeMgr();
                    Commande cm = cmgr.rechercher(idcmd);
                    
                    LignecommandeMgr lcg = new LignecommandeMgr();
                     int total = lcg.montant(idcmd);
              %>
        
        <section id="do_action">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Orders details</li>
				</ol>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<label>Cmd Nun :</label>
								<label><%=cm.getIdcmd()%></label>
							</li>
							<li>
								<label>Date : </label>
								<label><%=cm.getDatecmd()%></label>
							</li>
							<li>
								<label>Payment method :</label>
								<label><%=cm.getMethodePay()%></label>
							</li>
						</ul>
						<ul class="user_option">
							<%
                                                            
                                                        if (cm.getEtat().equals("in progress...")||cm.getEtat().equals("expedite")) {
                                                             ExpeditionMgr expmg = new ExpeditionMgr();
                                                           Expedition ex = expmg.rechercher(idcmd);
                                                         %>
                                                        
							<li class="single_field">
								<label> Mean of expediton :</label>
								<label><%=ex.getMoyenExpedition()%> </label>
							
							</li>
							<li class="single_field zip-field">
								<label>Expediton Num :</label>
								<label><%=ex.getNumeroExpedition()%> </label>
							</li>
                                                        <li class="single_field zip-field">
								<label>State :</label>
								<label><%=ex.getEtat()%> </label>
							</li>
                                                         <%            
                                                             }else{

                                                      %>
                                                      <li class="single_field">
								<label>Info :</label>
                                                                <label style="color: red">No information</label>
							</li> 
                                                      <%            
                                                             }

                                                      %>
						</ul>
						
					</div>
				</div>
				
			</div>
		</div>
	</section><!--/#do_action-->
            
          <section id="cart_items">
		<div class="container">
			
                    
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
						</tr>
					</thead>
                                        <%
                                           
                                           ArrayList<Lignecommande> lgc = new ArrayList();
                                           lgc = lcg.listr(idcmd);
                                            for (Lignecommande lc : lgc) {
                                               
                                                int idp = lc.getIdp();
                                         
                                                ProduitMgr pmgr = new  ProduitMgr();
                                                
                                                Produit p = pmgr.research(idp);
                                                ImageMgr mrg = new ImageMgr();
                                                Image img = mrg.rechercher(p.getIdp());
                                            %>
					<tbody>
                                           
						<tr>
							<td class="cart_product">
                                                            <a href=""><img src="<%=img.getUrl()%>" width="50px" height="50px" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""> <%=p.getLibelle() %></a></h4>
                                                                
							</td>
							<td class="cart_price">
								<p><%=p.getPrix() %></p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<input class="cart_quantity_input" type="text" name="quantity" value="<%=lc.getQte()%>" autocomplete="off" size="2">
								</div>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="/myMedicineApp/ManageCart?action=supp&idp=<%=p.getIdp()%>"><i class="fa fa-times"></i></a>
							</td>
						</tr>

						<% } %>
					</tbody>
				</table>
			</div>
                                       
		</div>
	</section> <!--/#cart_items-->
        <%                                                           
         if (cm.getEtat().equals("expedite")) {
         %>
        <section id="do_action">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-6">
					
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
                                                    <li>The amount total of the commande <span><%=total%> $</span></li>
							
                                                        
						</ul>
							
							<a class="btn btn-default check_out" href="/myMedicineApp/Rec?idcmd=<%=cm.getIdcmd()%>" class="btn btn-primary">Confirm Reception</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	
        <% } %>
          <jsp:include page="/views/footer.jsp"></jsp:include>

    </body>
</html>
