<%-- 
    Document   : pagne
    Created on : 29 déc. 2020, 17:31:07
    Author     : Administrateur
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="controller.ProduitMgr"%>
<%@page import="model.Produit"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>admin-&gt;userList</p>
        <p>&nbsp;</p>

        <p>Courses List:</p>
        <table width="948" height="233" border="1">
            <tr>
                <td>course ID</td>
                <td>course name</td>
                <td>course type</td>
                <td>course credit</td>
                <td>course hour</td>
                <td>delete</td>
            </tr>

            <%
                ArrayList<Produit> l1 = new ArrayList();
            ProduitMgr cmg = new ProduitMgr();
            l1 = cmg.list();

            for (Produit p : l1) {

                String cat = "";
                switch (p.getIdcat()) {
                    case 1:
                        cat = "Respiratory";
                        break;
                    case 2:
                        cat = "Analgesics";
                        break;
                    case 3:
                        cat = "Gastrointestinal";
                        break;
                    case 4:
                        cat = "Circulatory";
                        break;
                    case 5:
                        cat = "Eye/ear";
                        break;
                    case 6:
                        cat = "Oral hygienne";
                        break;
                    case 7:
                        cat = "Amtiparasitic";
                        break;
                }
                    
                   
            %>
            <tr>

                <td align="center"><%=p.getIdp()%></td>
                <td align="center"><%=p.getLibelle()%></td>
                <td align="center"><%=cat%></td>
                <td align="center"><%=p.getPrix()%></td>  
                <td align="center">
                    <a href="CourseServlet?operation='del'&courseID='<%=p.getIdp()%>'" onclick='return window.confirm("This course will be deleted. Are you sure?")'>delete</a>
                </td>

            </tr>
            <%
            }
            %>
        </table>
        <p>&nbsp;</p>
    </body>
</html>
