<%-- 
    Document   : login
    Created on : 14 déc. 2020, 19:01:37
    Author     : Administrateur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
        <% 
           
            if (request.getAttribute("mgs")!=null) {
                
        %>
        <h1 style="color: red"><%=request.getAttribute("mgs")%></h1>
         <%
                }
        %>
        <section id="form"><!--form-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                        <div class="login-form"><!--login form-->
                            <h2>Login to your account</h2>
                            <form action="LoginServlet" method="post">
                                <input type="emal" name="email" placeholder="Email" />
                                <input type="password" name="pwd" placeholder="Password" />
                                <span>
                                    <input type="checkbox" class="checkbox"> 
                                    Keep me signed in
                                </span>
                                <button type="submit" class="btn btn-default">Login</button>
                            </form>
                        </div><!--/login form-->
                    </div>
                    <div class="col-sm-1">
                        <h2 class="or">OR</h2>
                    </div>
                    <div class="col-sm-4">
                        <div class="signup-form"><!--sign up form-->
                            <h2>New User Signup!</h2>
                            <form action="RegisterServlet" method="post">
                                <input type="text" name="name" placeholder="Name"/>
                                <input type="text" name="fname" placeholder="First Name"/>
                                <input type="email" name="email" placeholder="Email Address"/>
                                <input type="password" name="pwd" placeholder="Password"/>
                                <button type="submit" name="signup" class="btn btn-default">Signup</button>
                            </form>
                        </div><!--/sign up form-->
                    </div>
                </div>
            </div>
        </section><!--/form-->
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
