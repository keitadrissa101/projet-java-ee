<%-- 
    Document   : ListProduct
    Created on : 14 déc. 2020, 18:53:12
    Author     : Administrateur
--%>

<%@page import="controller.ProduitMgr"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Produit"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="views/header.jsp"></jsp:include>
            <section id="cart_items">
                <div class="container">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Product List</li>
                        </ol>
                    </div>
                    <div class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="image">Item</td>
                                    <td class="description"></td>
                                    <td class="price">Price</td>
                                    <td class="quantity">Quantity</td>
                                    <td class="total">Action</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            <%
                                ArrayList<Produit> l = new ArrayList();
                                ProduitMgr cmg = new ProduitMgr();
                                l = cmg.list();

                                for (Produit p : l) {

                                    String cat = "";
                                    switch (p.getIdcat()) {
                                        case 1:
                                            cat = "Respiratory";
                                            break;
                                        case 2:
                                            cat = "Analgesics";
                                            break;
                                        case 3:
                                            cat = "Gastrointestinal";
                                            break;
                                        case 4:
                                            cat = "Circulatory";
                                            break;
                                        case 5:
                                            cat = "Eye/ear";
                                            break;
                                        case 6:
                                            cat = "Oral hygienne";
                                            break;
                                        case 7:
                                            cat = "Amtiparasitic";
                                            break;
                                    }


                            %>
                            
                            <tr>
                                <td class="cart_product">
                                    <a href=""><img src="images/cart/one.jpg" width="15%" alt=""></a>
                                </td>
                                <td class="cart_description">
                                    <h4><a href=""><%=p.getLibelle()%></a></h4>
                                </td>
                                <td class="cart_price">
                                    <p>$<%=p.getPrix()%></p>
                                </td>
                                <td class="cart_quantity">
                                    <input class="cart_quantity_input" type="text" name="quantity" value="<%=p.getQtestck()%>" autocomplete="off" size="2">
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href="">Add</i></a>
                                    <a class="cart_quantity_delete" href="">Edit</i></a>
                                    <a class="cart_quantity_delete" href="">Add</i></a>
                                    <a class="cart_quantity_delete" href="">Supply</i></a>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
        </section> <!--/#cart_items-->
        <jsp:include page="views/footer.jsp"></jsp:include>
    </body>
</html>
